//
//  PlayerClass.m
//  AuraApp
//
//  Created by Serge Gorbachev on 4/26/11.
//  Copyright 2011 Rosberry. All rights reserved.
//
#import "PlayerClass.h"
#include "WaveGen.h"
#import <UIKit/UIKit.h>

//#define SIZE 8192 // audio buffer size
const int samplingRate = 44100;
const size_t channelCount = 2; // channels count. Stereo
const size_t SIZE = 44100*channelCount; // audio buffer size.
const float sampleSeconds = SIZE / ((float)samplingRate * channelCount); // audio buffer size. 1 stereo seconds
const size_t NFORM = 20; // this complex sample sound will consist in 20 mixed tones

static BOOL isBowl = NO;
static double lastPow = -1;
static double lastHZ = -1;

@interface PlayerClass ()

@property (nonatomic, assign) NSInteger curentPow;

@end

@implementation PlayerClass
@synthesize listOfHerz,nav,sender;
@synthesize arrayF;

@synthesize count;
@synthesize stop;
@synthesize powAr;
@synthesize full;
@synthesize curentPow = _curentPow;


-(id)init
{
    self=[super init];
    if (self) {
        
        self.listOfHerz=[NSMutableArray array];
        self.powAr=[NSMutableArray  arrayWithObjects:@"500",@"7800",@"3100",@"8300",@"1400",@"2500", nil];
        
        self.curentPow=2;
    
        [self initAudioChannelsForAvatarFunctionality];
    }
    return self;
}

- (void)initAudioChannelsForAvatarFunctionality
{
    self.rightVolume = [[NSUserDefaults standardUserDefaults] floatForKey:@"rightChannelVolume"];
    self.leftVolume = [[NSUserDefaults standardUserDefaults] floatForKey:@"leftChannelVolume"];
}

-(void)dealloc
{
	[self stopMyTimer];
	[self killPlayer];
}

-(void)killPlayer
{
	/*NSError *error = nil;
	[self._player stop:&error];
	if( error )
	{
		NSLog( @"Cannot stop _player :'< : %@", error.localizedDescription);
	}
	self._player = nil;*/
}
/*
-(void)soundOf528PlayLong
{
    count = 0;
    stop = 15;
    [self playSoundOf528];
}

-(void)stopOf528Play
{
    stop = 1;
    [self stopMyTimer];
}*/

/*-(void)SoundOf528
{
    [self SoundOf528WithDuration:-1.0];
}
*/
/*-(void)SoundOf528WithDuration:(CGFloat)duration
{
	[self stopMyTimer];
	[self.listOfHerz removeAllObjects];

	srand(time(0));
    
    if (!(duration > 0))
    {
        stop = (arc4random() % 5)+5;
    }
    else
    {
        stop = duration;
    }
    
    [self.listOfHerz addObject:@(528.0f)];
    count=0;
	self.tm2 = [NSTimer scheduledTimerWithTimeInterval:1
												target:self
											  selector:@selector(playSoundOf528)
											  userInfo:nil
											   repeats:YES];
}*/

/*-(void)scanOfSoundWithLove
{
	[self stopMyTimer];

    [self.listOfHerz addObject:@(528.0f)];
    count=0;
    srand(time(0));  
    stop = (arc4random() % 4)+3;
	self.tm2=[NSTimer scheduledTimerWithTimeInterval:1
											  target:self
											selector:@selector(playSoundOf528)
											userInfo:nil
											 repeats:YES];
}*/

/*-(void)playSoundOf528
{
    if (count==0) {
        [self killPlayer];
        id<A440Player> player1 = [[A440AudioQueue alloc] initWithHerz:528];
        self._player = player1;
        [self playPlayer];
    }
    if (count>=stop) {
        [self stopMyTimer];
        return;
    }
    count++;
}*/

-(void)scanOfSound 
{
	if( self.tm2 )
	{
		[self stopMyTimer];
	}
	
    [self.listOfHerz addObject:@(528.0f)];
    count=0;
	
	self.tm2=[NSTimer scheduledTimerWithTimeInterval:1
											  target:self
											selector:@selector(playScanSound)
											userInfo:nil
											 repeats:YES];
}

/*-(void)playScanSound
{
	@autoreleasepool
	{
		if (count==0) {
			[self killPlayer];
			id<A440Player> player1 = [[A440AudioQueue alloc] initWithHerz:0];
			self._player = player1;
			[self playPlayer];
		}
		count++;
	}
}*/

/*-(void)playFibonachi
{
    if (count>=stop) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"stopPlay" object:self];
#ifdef CHAKRA_AURA_SCAN
        // To avoid crash
        [self.nav getHAEValue];
#else
        [self.nav playButton:sender];
        [self.nav getHAEValue];
#endif
		
        [self.tm2 invalidate], self.tm2=nil;
        [self stopPlayer];
        return;
    }
    
    if (cycl==[[self.arrayF objectAtIndex:curentTimeF] intValue]) {
        hz+=0.5;
       
        curentTimeF++;
        cycl=0;
        if (curentTimeF==[arrayF count]) {
            curentTimeF=0;
        }
        ((A440AudioQueue*)self._player).herz =hz;
    }
    
    
    cycl++;
    count++;//20;
}

-(void)playTrack
{
    count++;
    if (count>=stop) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"stopPlay" object:self];
#ifdef CHAKRA_AURA_SCAN
        // To avoid crash
        [self.nav getHAEValue];
#else
        [self.nav playButton:sender];
        [self.nav getHAEValue];
#endif
		[self.tm2 invalidate], self.tm2=nil;
		[self.audioPlayerLeft stop];
        self.audioPlayerLeft=nil;
        return;
    }
}

*/
// Plays Balance RX list
- (void) playListOfSound:(NSArray*)arOfHerz timer:(int)min sendButton:(UIButton*) but
{
    [self initAudioChannelsForAvatarFunctionality];
    
	self.curentPow = 0;
	self.sender = but;
	
	[self stopMyTimer];
	self.listOfHerz = [arOfHerz mutableCopy];
	
	min = min ? : 9001; // more that 9000 minutes for solitone mode
	stop=(min*60)/sampleSeconds;
	count=0;
	cycl = stop / ( sampleSeconds*self.powAr.count );
	curentHerz = 1;
	self.tm2=[NSTimer scheduledTimerWithTimeInterval:sampleSeconds
											  target:self
											selector:@selector(playList)
											userInfo:nil
											 repeats:YES];
	[self.tmStopPlaying invalidate];
	self.tmStopPlaying = [NSTimer scheduledTimerWithTimeInterval:min*60
														  target:self
														selector:@selector(stopPlaylist)
														userInfo:nil
														 repeats:NO];
}


- (void) stopPlaylist
{
	[self stopMyTimer];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopPlay" object:self];
}

- (void) playList
{
	if (count>=stop)
	{
		NSLog( @"Warning: playing longer than expected.");
	}
	
	oalQueuePlayback *player = [oalQueuePlayback sharedInstance];
	if( player.isPaused )
	{
		return;// skip if paused
	}
	
	
//	static dispatch_queue_t queueForAudioBufferCreation = dispatch_queue_create(
//			"com.quantum.audiobuffercreation",
//			NULL);
	static dispatch_queue_t queueForAudioBufferCreation = dispatch_get_main_queue();
	if (count%cycl==0)
	{
		dispatch_async( queueForAudioBufferCreation, ^
		{
			[self newBuffer:YES];
		});
	}
	else
	{
		dispatch_async( queueForAudioBufferCreation, ^
		{
			[self newBuffer:NO];
		});
	}

	++count;
}

-(void)newBuffer
{
	return [self newBuffer:YES];
}

-(void)newBuffer:(BOOL)changeHz
{
	static TWave wave = TWave(2, samplingRate);
	// create the 'wave' object (2-channels, sample rate)
	
	double Pow[NFORM];
	double Hz[NFORM];// vectors for Power and frequencies (Pow, Hz)
	short Buffer[SIZE];// audio buffer to be played
	
	memset(Hz, 0, NFORM*sizeof(double));
	memset(Pow, 0, NFORM*sizeof(double));

	for (int i=0; i<[self.listOfHerz count] && i<NFORM; i++) {
        if ([self.listOfHerz[i] isKindOfClass:[NSArray class]] || [self.listOfHerz[i] isKindOfClass:[NSMutableArray class]])
        {
            id herz = [self.listOfHerz firstObject];
            if ([herz doubleValue]<1000) {
                Hz[i]=[herz doubleValue]*6;
            }
        } else {
            if ([[self.listOfHerz objectAtIndex:i] doubleValue]<1000) {
                Hz[i]=[[self.listOfHerz objectAtIndex:i] doubleValue]*6;
            }
        }
	}
	
	for( int i=0; i<[self.powAr count]; ++i )
	{
        if ([self.powAr[i] isKindOfClass:[NSArray class]] || [self.powAr[i] isKindOfClass:[NSMutableArray class]])
        {
            Pow[i] = [[self.powAr[i] firstObject] doubleValue]*1.0*1;
        } else {
            Pow[i] = [self.powAr[i] doubleValue]*1.0*1;
        }
	}
	
	
	if([self.listOfHerz count] == 0){
		Hz[0] = arc4random()%20000 + 3000;
		Pow[0] = arc4random()%20000 + 3000;
	}
	
	if( changeHz || !isBowl )
	{
		self.curentPow++;
	}
	
	if (self.curentPow==[self.powAr count]) {
		self.curentPow=0;
	}

	int HzCount = self.listOfHerz.count ?: 1;
	if( HzCount > NFORM )
	{
		HzCount = NFORM;
	}

	// don't re-init bowl if settings wasn't changed
	if( isBowl )
	{
		if( lastHZ!=Hz[0] )
		{
			lastPow=Pow[self.curentPow];
			lastHZ=Hz[self.curentPow%HzCount];
			wave.Bowl( lastPow, lastHZ, 0.2, 0.02, 3, true);
		}
		
		wave.genBowl((short*)Buffer, SIZE);
	}
	else
	{
		if( lastPow!=Pow[self.curentPow] || lastHZ!=Hz[self.curentPow%HzCount] )
		{
			lastPow=Pow[self.curentPow];
			lastHZ=Hz[self.curentPow%HzCount];
			int offset = ( self.curentPow<[self.powAr count]-HzCount )
					?self.curentPow
					:self.curentPow%HzCount;
			wave.Set(&Pow[offset],Hz,NULL,HzCount);
			// fill the power and Hz with appropriate values
		}
		wave.gen((short*)Buffer, SIZE); 	// generate the wave in the audio buffer using
	}

    BOOL isAvatarEnabled = ((self.rightVolume > 0.0) || (self.leftVolume > 0.0));
    if (isAvatarEnabled)
    {
        for(int sampleIdx = 0; sampleIdx < SIZE/2; sampleIdx++)
        {
            Buffer[2*sampleIdx] = Buffer[2*sampleIdx] * self.rightVolume;
            Buffer[2*sampleIdx + 1] = Buffer[2*sampleIdx + 1] * self.leftVolume;
        }
    }
	
	[self playOpenALPlayerWithBuffer:Buffer];
}

#pragma mark - OpenAL player (new player)

- (void) playOpenALPlayerWithBuffer:(short *)rawBuffer
{
	if( !rawBuffer )
	{
		[self playOpenALPlayerFromURL];
		return;
	}
	
	oalQueuePlayback *player = [oalQueuePlayback sharedInstance];
    [player setupSource];
	if( [player isNewBufferNeeded] )
	{
		[player addNewBuffer:(void *)rawBuffer
				  withLength:SIZE*sizeof(short)
				withChannels:channelCount
		  withBitResoliution:16
			withSamplingRate:samplingRate];
		
		if( [player isNewBufferNeeded] )
		{ // leads to a recursive adding buffers
			__weak __typeof(self) weakSelf = self;
			dispatch_async(dispatch_get_current_queue(), ^
			{
				if( weakSelf )
				[weakSelf playList];
			});
		}
	}
	else
	{
		[player startPlaying];
		[player logSourceInfo];
		[player cleanupProcessedBuffers];
	}
}

- (void) playOpenALPlayerFromURL
{
	oalQueuePlayback *player = [oalQueuePlayback sharedInstance];
	if( [player isNewBufferNeeded] )
	{
		NSURL *urlToNewBuffer = [self saveAudioToCacheFolderWithData:full
												shouldUseCacheFolder:NO
															   error:nil];
		
		[player addNewBuffer:urlToNewBuffer];
		if( [player isNewBufferNeeded] )
		{ // leads to a recursive adding buffers
			[self playList];
		}
	}
	else
	{
		[player startPlaying];
		[player logSourceInfo];
		[player cleanupProcessedBuffers];
	}
}


- (void) stopOpenALPlayer
{
	oalQueuePlayback *player = [oalQueuePlayback sharedInstance];
	[player stopPlaying];
}

#pragma mark - AVQueuePlayer player (doesn't seems to work)

- (void) playQueuePlayer
{
	NSURL *urlToNewBuffer = [self saveAudioToCacheFolderWithData:full
											shouldUseCacheFolder:NO
														   error:nil];

	if( self.queuePlayer==nil )
	{
		self.queuePlayer = [[AVQueuePlayer alloc] initWithURL:urlToNewBuffer];
	}
	else
	{
		AVURLAsset *asset = [AVURLAsset assetWithURL:urlToNewBuffer];
		AVPlayerItem *newItem = [AVPlayerItem playerItemWithAsset:asset];
		BOOL canInsert = [self.queuePlayer canInsertItem:newItem afterItem:nil];
		if( canInsert )
		{
			[self.queuePlayer insertItem:newItem afterItem:nil];
		}
		else
		{
			[self.queuePlayer removeAllItems];
			[self.queuePlayer insertItem:newItem afterItem:nil];
		}
	}

	self.queuePlayer.volume = 1.0f;
	[self.queuePlayer play];
}

-(void)stopMyTimer
{
	[self.tm2 invalidate], self.tm2=nil;
	[self.tmStopPlaying invalidate], self.tmStopPlaying=nil;
	
    [self stopPlayer];
    [self.audioPlayerLeft stop];
    self.audioPlayerLeft=nil;
}

- (void)stopPlayer
{
    if( self.recording )
    {
        [self saveAudioToCacheFolderWithData:self.recording];
        self.recording = nil;
    }
    
    
    [self stopOpenALPlayer];
}


//! Just for Fun! while debugging audioplayer subsystem some sounds are very fun. Save it.
- (void) saveAudioToCacheFolderWithData:(NSData *)wavData
{
	[self saveAudioToCacheFolderWithData:wavData shouldUseCacheFolder:NO error:nil];
}

/** Save NSData to *.wav file
 *	@abstract Just for Fun! while debugging audioplayer subsystem some sounds are very fun. Save it.
 *	@param wavData WAVE audio data in a NSData object
 *	@param shouldUseCacheFolder will use Cashe folder if YES, Temp folder otherwise
 *	@param error returns error
 *	@return URL to a written file, if successful, nil otherwise
 */
- (NSURL *) saveAudioToCacheFolderWithData:(NSData *)wavData shouldUseCacheFolder:(BOOL)shouldUseCacheFolder error:(NSError *__autoreleasing*)error
{
	NSFileManager *fileMan = [NSFileManager defaultManager];

	// a path to root of caches( or Temp ) folder. Use caches folder is a good idea - it will be erased automaically, if system need it.
	NSString *path = nil;
	if( shouldUseCacheFolder )
	{
		path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
	}
	else
	{
		path = NSTemporaryDirectory();
	}

	// Create a folder if not exists
	NSString *folderToSave = [path stringByAppendingPathComponent:@"generatedSound"];
	BOOL folderExists = [fileMan fileExistsAtPath:folderToSave isDirectory:nil];
	if( !folderExists )
	{
		[fileMan createDirectoryAtPath:folderToSave withIntermediateDirectories:NO attributes:nil error:error];
		if( error && *error )
		{
			NSLog( @"creating a folder in Caches folder has encountered a error :'< : %@", (*error).localizedDescription);
			return nil;
		}
	}
	
	// find last track and create next filename
	NSDirectoryEnumerator *enumFolder = [fileMan enumeratorAtPath:folderToSave];
	NSString *lastPath = (NSString *)enumFolder.allObjects.lastObject;
	NSInteger trackNumber = 1;
	if( lastPath )
	{
		NSString *trackString = [[lastPath lastPathComponent] stringByDeletingPathExtension];
		trackNumber = [trackString integerValue] + 1;
	}
	NSString *trackName = [NSString stringWithFormat:@"%.4ld.wav", (long)trackNumber];
	
	// write to file
	NSString *fullpath = [folderToSave stringByAppendingPathComponent:trackName];
	[wavData writeToFile:fullpath options:0 error:error];
	if( error && *error )
	{
		NSLog( @"Writing WAV data to a folder has encountered a error :'< : %@", (*error).localizedDescription);
		return nil;
	}
	
	// return URL to just created file
	NSURL *urlToFile = [NSURL URLWithString:fullpath];
	return urlToFile;
}

+ (void) setAudioGenerationAlgorithmBowl:(BOOL)isBowl
{
	::isBowl = isBowl;
	[self reinitAudioGenerationAlgorithm];
}

+ (void) reinitAudioGenerationAlgorithm
{
	// force re-calculation
	::lastPow = -1;
	::lastHZ = -1;
}


@end
