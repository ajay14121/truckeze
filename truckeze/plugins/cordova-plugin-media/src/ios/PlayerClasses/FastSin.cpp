//------------------------------------------------------------------------------
// TFastSin
// calc y(n) = sin(n*W+B)
// W, B in RAD
// usage TFastSin fs(a,w,b)
//
#include <math.h>
#include "FastSin.hpp"

TFastSin::TFastSin(void)
{
 isInit=false;            
}
TFastSin::TFastSin(double a, double w, double b)
{ init(a,w,b); }

TFastSin::TFastSin(double amp, double Hz, double Samp, double phase) {
    init(amp, Hz, Samp, phase);
}

void TFastSin::init(double amp, double Hz, double Samp, double phase) { // init and save for reset
  isInit=true;
  cA=amp; cW=Freq2Inc(Hz,Samp); cB=phase;
  y0 = sin(-2*cW + cB);  y1 = sin(-cW + cB);  p = 2.0 * cos(cW);
  n=-1;   x=0;
}

void TFastSin::init(double a, double w, double b) { // init and save for reset
  isInit=true;
  cA=a; cW=w; cB=b;
  y0 = sin(-2*w + b);  y1 = sin(-w + b);  p = 2.0 * cos(w);
  n=-1;  x=0;
}

void TFastSin::reset(void) { // start n=0;
  init(cA,cW,cB);
}

// set amplitude.
void TFastSin::SetAmp(double amp)
{
 cA=amp;
}

// sin(wt+phase + x)
double TFastSin::calcSin(double px)
{
 n++;  x=n*cW;
 return cA*sin(cW*n+cB + px);
}

double TFastSin::calc(void)
{
  n++; x=n*cW;
  y2 = p*y1 - y0;
  y0 = y1;
  y1 = y2;
  return cA*y2;   // mutl by amp.
}

double TFastSin::Freq2Inc(double freq, double samp)
{
  return freq*2.*M_PI/samp;
}

// fill a short int mono buffer
void TFastSin::FillBuffer(short int*sbuff, int sb)
{
   for (int i=0; i<sb; i++) { //
      sbuff[i] =(short)calc();
   }
}

double TFastSin::initFade(double samp, double sec)
{
 return  xfade=Zfade=pow(10,-5/(samp*sec));
}

double TFastSin::Fade(void)
{
 xfade*=Zfade;
 return xfade;
}

