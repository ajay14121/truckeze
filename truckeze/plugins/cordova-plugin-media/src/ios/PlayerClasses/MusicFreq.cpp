//---------------------------------------------------------------------------
//  MusicFreq.cpp   27/08/2001    RFN
//	 Frequency <--> Musical notes conversor.
//	 class TMusicFreq
//---------------------------------------------------------------------------

#include <stdlib.h>
#include <math.h>
#include <float.h>
#include "MusicFreq.h"
#include "ColorScale.h"



static const char	*note_str[]={"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };
//---------------------------------------------------------------------------
// Freq2Color, convert freq to color
//---------------------------------------------------------------------------
COLORREF TMusicFreq::Freq2Color(double freq)
{
	int oct=Freq2Oct(freq); // get note and freq. err
    double f0=NoteOct2Freq(0,oct),
           fz=NoteOct2Freq(0,oct+1),
           ratio=(freq-f0)/(fz-f0);
           
    // ratio between RED, VIOLET
    COLORREF c=ColorScaleHSL(MakeRGB(0xff,0,0),MakeRGB(0xff,0,0xff),ratio);
    int r=(c&0xff0000)>>16, g=(c&0x00ff00)>>8, b=(c&0x0000ff); // swap red with blue
    COLORREF col=MakeRGB(r,g,b);
    return col;
}

// FreqInOctave. convert 'f' to octave 'o'
double TMusicFreq::FreqInOctave(double f, int o)
{
  double fb,ft;
  fb=NoteOct2Freq(0, o); ft=NoteOct2Freq(11, o); // rage freq in octave
  if (f>fb) for (;f && f>fb && !(f>=fb && f<=ft); f/=2);  // in octave
  else      for (;f && f<ft && !(f>=fb && f<=ft); f*=2);
  return f;
}


const char *TMusicFreq::NoteString(int i)
{
    static const char* bs="";
    if (i>=0 && i<=12) return (char*)note_str[i];
   else return bs;
}

const char *TMusicFreq::NoteString(double freq)
{
    static const char* bs="";
	int i=Freq2Note(freq);
	if (i>=0 && i<=12) return (char*)note_str[i];
   else return bs;
}


//---------------------------------------------------------------------------
// NoteOct2Freq, convert Note/oct to freq
//---------------------------------------------------------------------------
double TMusicFreq::NoteOct2Freq(int note, int oct)
{
	return (baseC0*pow(MUSICAL_INC,note+12.*oct));
}

// char* note of a freq
const char* TMusicFreq::Freq2StrNote(double freq)
{
	return NoteString(Freq2Note(freq));
}

//---------------------------------------------------------------------------
//	Freq2NoteOct, convert Freq to note/oct and error
// 0,  1  2   3  4  5   6  7   8  9  10 11
// C, C#, D, D#, E, F, F#, G, G#, A, A#, B
//---------------------------------------------------------------------------
const char* TMusicFreq::Freq2NoteOct( double freq )
{
	int n,o; char* no; 
   Freq2NoteOct( freq, n, o, no);
   return NoteString(n); // +char*(o); concatenate two strings
}

double TMusicFreq::NoteFit(double hz)
{
 int n,o; char* no; double err;
 Freq2NoteOct( hz, n, o, no, err);
 return NoteOct2Freq( n, o );
}

int TMusicFreq::Freq2NoteOct( double freq, int&note, int&oct, char*&NoteOct)
{
	int n,o; char* no; double err;
   Freq2NoteOct( freq, note, oct, NoteOct, err); n=note; o=oct; no=NoteOct;
   if (++n>11) {n=0; o++;} // freq of next note
   if (NoteOct2Freq( n, o ) - freq < err) { note=n; oct=o; NoteOct=const_cast<char *>(NoteString(n)); } // next note
   return oct*12+((oct>=0)?note:-note);   
}

int TMusicFreq::Freq2NoteOct( double freq, int&note, int&oct, char*&NoteOct, double&err )
{
	if (freq<=0) { note=oct=0; err=0;	return 0;  }
   // oct = floor( log2(freq/baseC0) )
   // note = baseC0 * MUSICAl_INC ^(note+12*oct)
   double lfB=log(freq)-LOG_baseC0;
   oct  = (int)floor( lfB/LOG2 );
   note = (int)( lfB/LOG_MUSICAL_INC - oct*12. );
   // char*
   // if (NoteOct!=NULL) NoteOct=char*(note_str[note])+((oct<0)?char*(oct):"+"+char*(oct));
   // error
   err = freq - NoteOct2Freq(note,oct);
   return oct*12+((oct>=0)?note:-note);
}

// Hz 2 octave
int TMusicFreq::Freq2Oct( double freq )
{
	if (freq<=0) return -999;
   return (int)floor( (log(freq)-LOG_baseC0)/LOG2 );
}

// Hz 2 note
int TMusicFreq::Freq2Note( double freq )
{
  if (freq<=0) return 0;
  return (int)( (log(freq)-LOG_baseC0)/LOG_MUSICAL_INC - Freq2Oct(freq)*12. );
}

// Error in note calc
double TMusicFreq::ErrInNote(double freq)
{
  return freq - NoteOct2Freq(Freq2Note(freq),Freq2Oct(freq));
}

// limit of freq in octave range
bool TMusicFreq::FreqInOctRange(double freq, int octDown, int octUp)
{
	if (freq<0 || freq>DBL_MAX/2)
   	return false;
 	int o=Freq2Oct(freq);
   return (o>=octDown) && (o<=octUp);
}

// convert a freq. to a element freq in octave -4
double TMusicFreq::Freq2Element(double freq)
{
   double fb=NoteOct2Freq(11, -4); // B(-4) upper limit
   if (freq==0) freq=0.01;
   if (freq>fb) for (; freq>fb; freq/=2.);
   else			 for (; freq<fb/2; freq*=2.);
   return freq;
}

// convert Hzfrm freq to 'oct'
void TMusicFreq::Frm2OCtaveRange(double *HzFrm,  int nform, int oct)
{
	for (int i=0; i<nform; i++)
    	HzFrm[i]=FreqInOctave(HzFrm[i], oct);
}
