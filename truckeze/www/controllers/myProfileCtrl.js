angular.module('truckeze.controllers').controller('myProfileCtrl', function($scope, $state, $interval, $http, $ionicPopup,ionicToast, $ionicLoading, $cordovaFileTransfer, $cordovaCamera, $localStorage, $cordovaFacebook, $rootScope, $ionicHistory) {
    $scope.user = $localStorage.user;
    console.log($scope.user);
    $scope.imgURIMenu = '';
    $scope.logout = function() {
        
        console.log("controller working");
        $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/user/logout",
                method: 'POST',
                headers: {
                    'Access-Token': $localStorage.user.token,
                    'Email': $localStorage.user.email
                }
            }).success(function(response){
                $ionicLoading.hide();
                console.log(response);
                $interval.cancel($rootScope.badgeInterval);
                $localStorage.$reset();
                $ionicHistory.clearCache();
                $ionicHistory.clearHistory();
                $state.go("main");
            }).error(function(error){
                $ionicLoading.hide();
                 console.log(error);
            });     
    };

    $scope.uploadTemporary = function() {
        var server = $rootScope.apiUrl + "API/user/edit";
        //var filePath = cordova.file.documentsDirectory + "testImage.png";
        console.log($scope.imgURIMenu);
        var filePath = $scope.imgURIMenu;
        var trustHosts = true;

        var options = {
            params: {
                image: $scope.imgURIMenu
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email,
            },
            fileKey: 'image'
        };
        try {
            $ionicLoading.show();
            $cordovaFileTransfer.upload(server, filePath, options)
                    .then(function(result) {
                        result = angular.fromJson(result);
                        response = angular.fromJson(result.response);
                        $ionicLoading.hide();
                        if (response.status == "1") {
                            ionicToast.show(response.message, 'bottom', true, 1000);
                            $localStorage.user.profile_image =  response.data.url;
                        } else {
                            ionicToast.show(response.message, 'bottom', true, 1000);
                        }
                        // Success!
                    }, function(err) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'failed',
                            template: 'API is not working'
                        });
                        // Error
                    }, function(progress) {
                        $ionicLoading.hide();
                        console.log(progress);
                        // constant progress updates
                    });

        } catch (e) {
            console.log('File Tarfer Plugin will work on device');
            consoel.log(e);
        }
    };
    $scope.setImage = function() {
        $ionicPopup.show({
            title: 'upload image',
            template: '',
            cssClass: 'upload-popup',
            buttons: [{
                    text: "<img src='img/camera-b.png' alt='image for camera' ng-click='openCamera()'><h5>Camera</h5>",
                    onTap: function() {
                        try {
                            var options = {
                                quality: 75,
                                destinationType: Camera.DestinationType.DATA_URL,
                                sourceType: Camera.PictureSourceType.CAMERA,
                                allowEdit: true,
                                encodingType: Camera.EncodingType.JPEG,
                                targetWidth: 300,
                                targetHeight: 300,
                                popoverOptions: CameraPopoverOptions,
                                saveToPhotoAlbum: false
                            };
                            $cordovaCamera.getPicture(options).then(function(imageData) {
                                console.log('Camera Capture Success');
                                console.log(imageData);
                                $scope.imgURIMenu = "data:image/jpeg;base64," + imageData;
                                $scope.uploadTemporary();
                            }, function(err) {
                                // An error occured. Show a message to the user
                                console.log('Camera Capture Error');
                                console.log(err);
                            });
                        } catch (e) {
                            console.log('Camera Plugin only work on device');
                        }
                    }
                },
                {
                    text: "<img src='img/sccreen-b.png' alt='image for gallery'  ng-click='openGallery()'><h5>Gallery</h5>",
                    onTap: function() {
                        try {
                            var options = {
                                quality: 50,
                                destinationType: Camera.DestinationType.FILE_URI,
                                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                                allowEdit: true,
                                targetWidth: 200,
                                targetHeight: 200

                            };

                            $cordovaCamera.getPicture(options).then(function(imageURI) {
                                console.log('Camera getPicture Success');
                                console.log(imageURI);
                                $scope.imgURIMenu = imageURI;
                                $scope.uploadTemporary();
                            }, function(err) {
                                // error
                                console.log('Camera getPicture Error');
                                console.log(err);
                            });
                        } catch (e) {
                            console.log('Camera Plugin only work on device');
                        }
                    }
                }]
        });
    };
});
