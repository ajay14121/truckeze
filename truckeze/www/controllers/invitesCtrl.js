angular.module('truckeze.controllers').controller('invitesCtrl', function($scope, $state, $http, $rootScope, $cordovaContacts, $stateParams, $localStorage, $ionicLoading, $ionicPopup,ionicToast) {
    $scope.invitesLoad = function(){
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/projects/invites",
            method: 'POST',
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            if (response.status == '1') {
                console.log(response.data);
                $scope.invites =  response.data;
                console.log($scope.invites);
                if(response.data.length==0)
                {
                    ionicToast.show('No Invitation','bottom', true, 1000);
                }
            } else {
                ionicToast.show(response.message,'bottom', true, 1000);
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            ionicToast.show('API is not working','bottom', true, 1000);
        });
    };
    
    $scope.acceptRejectInvite = function(invitation_id,status){
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/projects/update_users",
            method: 'POST',
            data:{
                'invitation_id':invitation_id,
                'status':status
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            if (response.status == '1') {
                $scope.invitesLoad();
            } else {
                ionicToast.show(response.message,'bottom', true, 1000);
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            ionicToast.show('API is not working','bottom', true, 1000);
        });
    };
    $scope.invitesLoad();
});

