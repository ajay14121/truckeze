angular.module('truckeze.controllers').controller('menuCtrl', function ($scope, $state, $http,$ionicPopup,$ionicLoading,$cordovaFileTransfer, $cordovaCamera, $localStorage, $cordovaFacebook, $rootScope, $ionicHistory) {
    $rootScope.user = $localStorage.user;
    $scope.logout = function () {
        $localStorage.$reset();
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
        $state.go("login");
        console.log("controller working");


        try {
            $cordovaFacebook.logout()
                    .then(function (success) {
                        // success
                        console.log('Facebook Logout Success');
                    }, function (error) {
                        // error
                        console.log('Facebook Logout Fail');
                    });
        } catch (e) {
            console.log('Facebook Logout only work on Device');
            console.log(e);
        }
    };
    $scope.uploadTemporary = function() {
        var server = $rootScope.apiUrl + "API/User/upload_record_file";
        //var filePath = cordova.file.documentsDirectory + "testImage.png";
        var filePath = $scope.imgURIMenu;
        var trustHosts = true;
        var options = {
            params: {
                type: 'image'
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email,
            }
        };
        try {
            $ionicLoading.show();
            $cordovaFileTransfer.upload(server, filePath, options)
                    .then(function(result) {
                        result = angular.fromJson(result);
                        response = angular.fromJson(result.response);

                        console.log('file upload success');
                        console.log(response);
                        $ionicLoading.hide();
                        if (response.status) {
                            $scope.imgURIMenuTemp = response.data.filename;
//                            var alertPopup = $ionicPopup.alert({
//                                title: 'Congratulations!',
//                                template: 'You have uploaded your client image successfully.',
//                                okText:'Continue'
//                            }).then(function(){
                                $scope.uploadToServer();
//                            });
                            
                        } else {
                            $ionicPopup.alert({
                                title: 'Upload Image Failed Temporary',
                                template: response.message
                            });
                        }
                        // Success!
                    }, function(err) {
                        $ionicLoading.hide();
                        console.log('file upload error');
                        console.log(err);
                        var alertPopup = $ionicPopup.alert({
                            title: 'API Error',
                            template: 'API is not working'
                        });
                        // Error
                    }, function(progress) {
                        //$ionicLoading.hide();
                        console.log(progress);
                        // constant progress updates
                    });

        } catch (e) {
            console.log('File Tarfer Plugin will work on device');
            consoel.log(e);
        }
    };
    $scope.uploadToServer=function()
    {
           if($scope.imgURIMenuTemp== undefined)
        {
            var alertPopup = $ionicPopup.alert({
                title: 'Oops',
                template: 'Please upload a current photo of your client '
            });
        }
        else{
        //$ionicLoading.show();
        $scope.popup = $ionicPopup.show({
            template:'<div class="text-center" style="width:60%;margin:0 auto"><ion-spinner class="bubbles"></ion-spinner><p>Uploading image.....</p></div>',
            delay:'10000',
            cssClass:'scan-popup'
        });
        $http({
            url: $rootScope.apiUrl + "API/User/update_user",
            method: 'POST',
            data: {
                image: $scope.imgURIMenuTemp
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {            
            //$ionicLoading.hide();
            console.log('Record Updated');
            console.log(response);
            if (response.status) {
                 $scope.popup.close();
                  var alertPopup = $ionicPopup.alert({
                                title: 'Congratulations!',
                                template: 'You have uploaded your image successfully.',
                                okText:'Ok'
                            });
            } else {
                $ionicPopup.alert({
                    title: "Error",
                    template: response.message
                });
            }
        }).error(function(erorr) {
            console.log('Record Updated');
            console.log(erorr);
            $scope.popup.close();
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'failed',
                template: 'API is not working'
            });
        });
    }
    };
    $scope.setImage = function () {
        $ionicPopup.show({
            title: 'upload image',
            template: '',
            cssClass: 'upload-popup',
            buttons: [{
                    text: "<img src='img/camera-b.png' alt='image for camera' ng-click='openCamera()'><h5>Camera</h5>",
                    onTap: function () {
                        try {
                            var options = {
                                quality: 75,
                                destinationType: Camera.DestinationType.DATA_URL,
                                sourceType: Camera.PictureSourceType.CAMERA,
                                allowEdit: true,
                                encodingType: Camera.EncodingType.JPEG,
                                targetWidth: 300,
                                targetHeight: 300,
                                popoverOptions: CameraPopoverOptions,
                                saveToPhotoAlbum: false
                            };
                            $cordovaCamera.getPicture(options).then(function (imageData) {
                                console.log('Camera Capture Success');
                                console.log(imageData);
                                $scope.imgURIMenu = "data:image/jpeg;base64," + imageData;
                                $scope.uploadTemporary();
                            }, function (err) {
                                // An error occured. Show a message to the user
                                console.log('Camera Capture Error');
                                console.log(err);
                            });
                        } catch (e) {
                            console.log('Camera Plugin only work on device');
                        }
                    }
                },
                {
                    text: "<img src='img/sccreen-b.png' alt='image for gallery'  ng-click='openGallery()'><h5>Gallery</h5>",
                    onTap: function () {
                        try {
                            var options = {
                                quality: 50,
                                destinationType: Camera.DestinationType.FILE_URI,
                                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                                allowEdit: true,
                                targetWidth: 200,
                                targetHeight: 200

                            };

                            $cordovaCamera.getPicture(options).then(function (imageURI) {
                                console.log('Camera getPicture Success');
                                console.log(imageURI);
                                $scope.imgURIMenu = imageURI;
                                $scope.uploadTemporary();
                            }, function (err) {
                                // error
                                console.log('Camera getPicture Error');
                                console.log(err);
                            });
                        } catch (e) {
                            console.log('Camera Plugin only work on device');
                        }
                    }
                }]
        });
    };
});
    
