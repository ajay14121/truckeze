angular.module('truckeze.controllers').controller('invitationCtrl', function($scope, $state, $http, $rootScope, $cordovaContacts, $stateParams, $localStorage, $ionicLoading, $ionicPopup) {

    $scope.contactsList = [];
    $scope.selectedUserList = [];
    $scope.inviteList = [];
    $scope.emailString = '';
    $scope.nameString = '';
    $scope.showClear = false;
    console.log("invitation control");
    $scope.displayContacts = function() {
        var options = {};
        options.multiple = true;
        
        try{
            $cordovaContacts.find(options).then(function(allContacts) {
               for (var index = 0; index < allContacts.length; index++){
                    console.log(allContacts[index]);
                    if (allContacts[index].emails && allContacts[index].name.formatted){
                        var object = {};
			object.name = allContacts[index].name.formatted;
		    	object.email = allContacts[index].emails[0].value;
			object.selected = false;
                        $scope.contactsList.push(object);
		    }
                }
            }); 
        }catch(e){
            console.log(e);
        }
        
    };

    $scope.sendInvitation = function() {
        if ($scope.selectedUserList.length == 0) {
            $ionicPopup.alert({
                title: 'Invalid Action',
                template: 'Please Select Atleast One Player'
            });
        } else {
            $scope.emailString = '';
            $scope.nameString = '';
            for (index = 0; index < $scope.inviteList.length; index++)
            {
                if (index == $scope.inviteList.length - 1)
                {
                    $scope.emailString += $scope.inviteList[index].email;
                    $scope.nameString += $scope.inviteList[index].name;
                } else {
                    $scope.emailString += $scope.inviteList[index].email + ',';
                    $scope.nameString += $scope.inviteList[index].name + ',';
                }
            }
            alert($scope.emailString);
            alert($scope.nameString);
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/projects/invitation",
                method: 'POST',
                data: {
                    "email": $scope.emailString,
                    "project_id": $stateParams.project_id,
                    "name": $scope.nameString   
                },
                headers: {
                    'Access-Token': $localStorage.user.token,
                    'Email': $localStorage.user.email
                }
            }).success(function (response) {
                //alert(response.status);
                $ionicLoading.hide();
                if (response.status == '1') {
                    $ionicPopup.show({
                        title: "Success",
                        template: response.message,
                        buttons: [
                            {
                                text: "ok",
                                onTap: function () {
                                    $scope.clearAll();
                                    $state.go("home", {project_id: $stateParams.project_id});
                                }
                            }
                        ]
                    });


                } else {
                    $ionicPopup.alert({
                        title: "Failed",
                        template: response.message
                    });
                }
            }).error(function(erorr) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Failed',
                    template: 'API is not working'
                });
            });
        }
    };
    $scope.selectAll = function()
    {
        $scope.selectedUserList.splice(0, $scope.selectedUserList.length);
        for (index = 0; index < $scope.contactsList.length; index++)
        {
            $scope.contactsList[index].selected = true;
            $scope.selectedUserList.push({"index": index, "email": $scope.contactsList[index].email});
            $scope.inviteList.push({"email": $scope.contactsList[index].email, "name": $scope.contactsList[index].name});
        }
        $scope.showClear = true;
    };
    $scope.clearAll = function ()
    {
        $scope.showClear = false;
        for (index = 0; index < $scope.contactsList.length; index++)
        {
            $scope.contactsList[index].selected = false;

        }
        $scope.selectedUserList.splice(0, $scope.selectedUserList.length);
        $scope.inviteList.splice(0, $scope.inviteList.length);
    };
    $scope.currentStatus = function (selection, email, name, index)
    {
        //alert(selection);
        if (selection == false)
        {
            $scope.selectedUserList.splice(index, 1);
            $scope.inviteList.splice(index, 1);
            //alert("element deleted"+index);
        }
        if (selection == true) {
            $scope.selectedUserList.push({"index": index, "email": email});
            $scope.inviteList.push({"email": email, "name": name});
        }
    };


    $scope.displayContacts();
});

