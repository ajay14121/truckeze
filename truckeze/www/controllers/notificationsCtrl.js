angular.module('truckeze.controllers').controller('notificationsCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading, $localStorage) {
     $scope.unreadNotifications='';
    $scope.displayNotifications = function()
    {
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/notification/list",
            method: 'POST',
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            if (response.status == '1') {
                $scope.notifications = response.data;
                $scope.unreadNotifications='';
                for(var index=0;index<$scope.notifications.length;index++)
                {
                    if($scope.notifications[index].is_read="0")
                    {
                        $scope.unreadNotifications+=$scope.notifications[index].notification_id+',';
                        
                    }
                }
                
                $scope.changeStatus($scope.unreadNotifications);
            } else {
                $ionicPopup.alert({
                    title: "Failed",
                    template: response.message
                });
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Failed',
                template: 'API is not working'
            });
        });

    };
    $scope.displayNotifications();
    $scope.changeStatus=function(unreadId)
    {
        unreadId=unreadId.substring(0,unreadId.length-1);
        //$ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/notification/update",
            method: 'POST',
            data: {
                notification_id:unreadId,
                status:'1'
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            //$ionicLoading.hide();
            if (response.status == '1') {
                console.log("status changed successfully");
            } else {
                console.log("status change failed");
            }
        }).error(function(erorr) {
            //$ionicLoading.hide();
            console.log("API is not working");
        });
    };
});
    
