angular.module('truckeze.controllers').controller('changePasswordCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading,$localStorage) {
    $scope.data = {};
    $scope.data.old_password = '';
    $scope.data.new_password = '';
    $scope.data.confirm_new_password = '';
    console.log($scope.data);
    $scope.changePassword = function() {
        
        if ($scope.data.new_password.length < 6) {
            $ionicPopup.alert({
                title: 'Invalid Password',
                template: 'New Password atleast six charaters long'
            });
        } else if ($scope.data.new_password !== $scope.data.confirm_new_password) {
            $ionicPopup.alert({
                title: 'Confirmation Error',
                template: 'Confirmation Password is not matching'
            });
        }  else {
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/update_password",
                method: 'POST',
                data: {
                    old_password: $scope.data.old_password,
                    new_password: $scope.data.new_password,
                },
                headers: {
                    'Access-Token': $localStorage.user.token,
                    'Email': $localStorage.user.email
                }
            }).success(function(response) {
                $ionicLoading.hide();
                if (response.status == "1") {
                    $ionicPopup.alert({
                        title: "Thank You",
                        template: response.message
                    }).then(function(res) {
                        $location.path("/app/setting");
                    });
                } else {
                    $ionicPopup.alert({
                        title: "Oops",
                        template: response.message
                    });
                }
            }).error(function(error) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Process failed',
                    template: 'API is not working'
                });
            });
        }
    };
});
    
