angular.module('truckeze.controllers').controller('forgetPasswordCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading) {
    $scope.data = {};
    $scope.data.email = '';
    console.log($scope.data);
    $scope.forgetPassword = function() {
        if ($scope.data.email === '') {
            $ionicPopup.alert({
                title: 'Empty Field',
                template: 'Please enter your email id'
            });
        } else if (!$rootScope.email_filter.test($scope.data.email)) {
            $ionicPopup.alert({
                title: 'Invalid Email',
                template: 'Please enter valid email'
            });
        } else {
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/forgot_password",
                method: 'POST',
                data: {
                    email: $scope.data.email,
                }
            }).success(function(response) {
                $ionicLoading.hide();
                if (response.status) {
                    $ionicPopup.alert({
                        title: "Thank You",
                        template: response.message
                    }).then(function(res) {
                        $location.path("/login");
                    });
                } else {
                    $ionicPopup.alert({
                        title: "Oops",
                        template: response.message
                    });
                }
            }).error(function(error) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Process failed',
                    template: 'API is not working'
                });
            });
        }
    };
});
    
