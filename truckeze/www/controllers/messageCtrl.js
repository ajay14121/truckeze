angular.module('truckeze.controllers').controller('messageCtrl', function($scope, $timeout, $localStorage, $stateParams, $interval, $http, $location, $ionicPopup, $rootScope, $ionicLoading, $ionicScrollDelegate, ionicToast) {
    $scope.data = {};
    $scope.hideTime = true;
    $scope.user_messages = [];
    $scope.myInterval = '';
    $scope.message_unread='';
    $scope.title = $stateParams.project_name;
    $scope.$on('$ionicView.enter', function() {
        $scope.messageCount=0;
        $scope.messagesLoad();
        $scope.myInterval = $interval(function() {
            $scope.messagesLoad();
        }, 2000);
    });
    $scope.$on('$ionicView.leave', function() {
        $interval.cancel($scope.myInterval);
    });
    $scope.messagesLoad = function() {
        console.log("message loading");
        console.log($stateParams.project_id);
        $scope.message_unread='';
        $http({
            url: $rootScope.apiUrl + "API/message/list",
            method: 'POST',
            data: {
                project_id: $stateParams.project_id
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            console.log(response.status);
                 if (response.status == '1') {
                old_messages = $scope.messages;
                $scope.messages = response.data;           
                $scope.message_unread='';
                if (response.data.length != old_messages.length) {
                    $ionicScrollDelegate.scrollBottom(true);
                }
                console.log($scope.messages.length);
                for(var index=0;index<$scope.messages.length;index++)
                {
                    console.log($scope.messages[index].is_read);
                    if($scope.messages[index].is_read=="0")
                    {
                        $scope.message_unread+=$scope.messages[index].msg_id+',';
                    }
                }
                console.log($scope.message_unread);
                  $scope.changeMsgStatus($scope.message_unread);
            } else {
                if($scope.messageCount==0){
                    ionicToast.show(response.message,'bottom', true, 1000);
                    $scope.messageCount++;
                }
               
           }
              
        }).error(function(erorr) {
            //$ionicLoading.hide();
            ionicToast.show('API is not working', 'bottom', true, 1000);
        });
    };



    $scope.sendMessage = function() {
        if ($scope.data.message.trim() == '') {
            return false;
        }
        //$ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/Message/send",
            method: 'POST',
            data: {
                type: 'group',
                message: $scope.data.message,
                send_to: $stateParams.project_id
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            //$ionicLoading.hide();
            if (response.status == '1') {
                $scope.messagesLoad();
                $scope.data.message='';
            } else {
                $ionicPopup.alert({
                    title: "Failed",
                    template: response.message
                });
            }
        }).error(function(erorr) {
            //$ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Failed',
                template: 'API is not working'
            });
        });
    };
    $scope.changeMsgStatus=function(message_unread)
    {
        message_unread=message_unread.substring(0,message_unread.length-1);
   
            //$ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/Message/update",
            method: 'POST',
            data: {
                msg_id:message_unread,
                status:'1'
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            //$ionicLoading.hide();
            if (response.status == '1') {
                console.log("status changed successfully");
            } else {
                console.log("status change failed")
            }
        }).error(function(erorr) {
            //$ionicLoading.hide();
            console.log("API is not working")
        });
        
    };

    $scope.inputUp = function() {
        if (isIOS)
            $scope.data.keyboardHeight = 216;
        $timeout(function() {
            $ionicScrollDelegate.scrollBottom(true);
        }, 300);

    };

    $scope.inputDown = function() {
        if (isIOS)
            $scope.data.keyboardHeight = 0;
        $ionicScrollDelegate.resize();
    };

    $scope.closeKeyboard = function() {
        // cordova.plugins.Keyboard.close();
    };


    $scope.data = {};
    $scope.myId = $localStorage.user.id;
    $scope.messages = [];

});


angular.module('stringflix').directive('input', function($timeout) {
    return {
        restrict: 'E',
        scope: {
            'returnClose': '=',
            'onReturn': '&',
            'onFocus': '&',
            'onBlur': '&'
        },
        link: function(scope, element, attr) {
            element.bind('focus', function(e) {
                if (scope.onFocus) {
                    $timeout(function() {
                        scope.onFocus();
                    });
                }
            });
            element.bind('blur', function(e) {
                if (scope.onBlur) {
                    $timeout(function() {
                        scope.onBlur();
                    });
                }
            });
            element.bind('keydown', function(e) {
                if (e.which == 13) {
                    if (scope.returnClose)
                        element[0].blur();
                    if (scope.onReturn) {
                        $timeout(function() {
                            scope.onReturn();
                        });
                    }
                }
            });
        }
    }
});
    
