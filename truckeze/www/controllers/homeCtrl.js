angular.module('truckeze.controllers').controller('homeCtrl', function ($scope, ionicToast, $interval, $stateParams, $http, $location, $state, $ionicPopup, $localStorage, $rootScope, $ionicLoading, $cordovaCapture, $cordovaCamera, $cordovaFileTransfer) {
    $scope.data = {};
    $scope.data.name = '';
    $scope.project_id = '';
    $scope.save = true;
    $scope.data.userList = [];
    $scope.data.fileURI = 'video uri';
    $scope.title = "New Project";
    $scope.disabled = false;
    $scope.readonly = '';
    $scope.videoURI = ''
    $scope.data.deadline = new Date();

    $scope.showContacts = function () {
        $state.go("invitations", {"project_id": $scope.project_id});
    };
    $scope.saveProject = function () {
        console.log($scope.data);
        var current_date_time = new Date();
        if ($scope.data.name == '') {

            ionicToast.show('Project name is mandatory', 'bottom', true, 1000);
        } else if (!$rootScope.name.test($scope.data.name)) {
           
            ionicToast.show('Only characters are allowed', 'bottom', true, 1000);
        } else if ($scope.data.deadline.getTime() <= current_date_time.getTime()) {
            ionicToast.show('Deadline should not be current date and time', 'bottom', true, 1000);
        } else {
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/Projects/create",
                method: 'POST',
                data: {
                    project_name: $scope.data.name,
                    deadline: $scope.data.deadline,
                },
                headers: {
                    'Access-Token': $localStorage.user.token,
                    'Email': $localStorage.user.email
                }
            }).success(function (response) {
                $ionicLoading.hide();
                if (response.status == '1') {
                    $scope.project_id = response.data.project_id;
                    $state.go("home", {project_id: $scope.project_id});
                } else {
                    ionicToast.show(response.message, 'bottom', true, 1000);
                }
            }).error(function (erorr) {
                $ionicLoading.hide();
                ionicToast.show('API is not working', 'bottom', true, 1000);
            });
        }
    };
    $scope.countUnread = function () {
         var i=0;
                       $rootScope.badgeInterval=$interval(function () {
                           
                        console.log("counting in progress"+i);
                        i++;
                        $http({
                            url: $rootScope.apiUrl + "API/user/count",
                            method: 'POST',
                            headers: {
                                'Access-Token': $localStorage.user.token,
                                'Email': $localStorage.user.email
                            }
                        }).success(function (response) {
                            if (response.status == '1') {
                                console.log(response.data);
                                $rootScope.notificationCount=response.data.notification_count;
                                $rootScope.messageCount=response.data.msg_count;
                                
                            } else {
                                console.log("failed");
                            }
                        }).error(function (erorr) {
                            console.log("api not working");
                        });
                    }, 2000);
                    
                };
    $scope.deleteFlix = function () {
       if ($scope.is_owner == "0")
        {
            ionicToast.show("you do not have permission to delete this project", 'bottom', true, 1000);
        } else {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Delete Flix',
                template: 'Are you sure you want to delete flix?'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    $ionicLoading.hide();
                    $http({
                        url: $rootScope.apiUrl + "API/projects/delete_project",
                        method: 'POST',
                        data: {
                            project_id: $scope.project_id
                        },
                        headers: {
                            'Access-Token': $localStorage.user.token,
                            'Email': $localStorage.user.email
                        }
                    }).success(function (response) {
                        $ionicLoading.hide();
                        if (response.status == '1') {
                            ionicToast.show("you have successfully deleted a project", 'bottom', true, 1000);
                            $state.go("projects");
                        } else {
                            ionicToast.show(response.message, 'bottom', true, 1000);

                        }
                    }).error(function (erorr) {
                        $ionicLoading.hide();
                        ionicToast.show("API is not working", 'bottom', true, 1000);
                    });
                } else {
                    console.log('You are not sure');
                }
            });
        }
    };
    $scope.$on('$ionicView.enter', function () {
        $scope.countUnread();
        if ($stateParams.project_id > 0) {
            $scope.project_id = $stateParams.project_id;
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/projects/project_details",
                method: 'POST',
                data: {
                    project_id: $stateParams.project_id
                },
                headers: {
                    'Access-Token': $localStorage.user.token,
                    'Email': $localStorage.user.email
                }
            }).success(function (response) {
                $ionicLoading.hide();
                if (response.status == '1') {
                    $scope.data.name = response.data.project_name;
                    $scope.data.deadline = response.data.deadline;
                    $scope.title = $scope.data.name;
                    $scope.data.userList = response.data.invited_users;
                    $scope.is_owner = response.data.is_owner;
                    $scope.save = false;

                } else {
                    ionicToast.show(response.message, 'bottom', true, 1000);
                }
            }).error(function (erorr) {
                $ionicLoading.hide();

                ionicToast.show('API is not working', 'bottom', true, 1000);
            });

        } else {
            $scope.is_owner = '1';
        }
    });

    $scope.setVideo = function () {
        $ionicPopup.show({
            title: 'upload video',
            template: '',
            cssClass: 'upload-popup',
            buttons: [{
                    text: "<img src='img/camera-b.png' alt='image for camera' ng-click='openCamera()'><h5>Camera</h5>",
                    onTap: function () {
                        try {
                            var options = {limit: 1, quality: 1};

                            $cordovaCapture.captureVideo(options).then(function (videoData) {
                                $scope.videoURI = videoData[0].fullPath;
                                $scope.uploadVideo();
                            }, function (err) {
                                // An error occurred. Show a message to the user
                            });
                        } catch (e) {
                            console.log("video capture will work on device only" + e);
                        }
                    }
                },
                {
                    text: "<img src='img/sccreen-b.png' alt='image for gallery'  ng-click='openGallery()'><h5>Gallery</h5>",
                    onTap: function () {
                        var options = {
                            quality: 50,
                            destinationType: Camera.DestinationType.FILE_URI, // <== try THIS
                            sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
                            mediaType: navigator.camera.MediaType.VIDEO
                        };

                        $cordovaCamera.getPicture(options).then(function (videoURI) {
                            $scope.videoURI = videoURI;
                            $scope.uploadVideo();
                        }, function (err) {
                            console.log("err", JSON.stringify(err));
                        });
                    }
                }]
        });
    };

    $scope.uploadVideo = function () {
        var server = $rootScope.apiUrl + "API/user/upload_video";
        //var filePath = cordova.file.documentsDirectory + "testImage.png";

        var filePath = $scope.videoURI;
        var trustHosts = true;
        var fileName =  'video.mov';
        var mimeType ="video/quicktime";
        
        if($rootScope.platform == 'Android' || $rootScope.platform == 'android'){
            fileName = 'video.mp4';
            mimeType = "video/mp4";
        } 
        var options = {
            params: {
                video: $scope.videoURI,
                project_id: $scope.project_id
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email,
            },
           
            fileKey: 'video',
            fileName:'abc.mov',
            mimeType:"video/quicktime",
        };
        try {
            $ionicLoading.show();
            $cordovaFileTransfer.upload(server, filePath, options)
                    .then(function (result) {
                        result = angular.fromJson(result);
                        response = angular.fromJson(result.response);
                        $ionicLoading.hide();
                        if (response.status == "1") {
                            $scope.data.fileURI=response.data.url;
                            ionicToast.show("video uploaded successfully", 'bottom', true, 1000);
                        } else {
                            
                            ionicToast.show(response.message, 'bottom', true, 1000);
                        }
                        // Success!
                    }, function (err) {
                        $ionicLoading.hide();
                        
                        ionicToast.show('API is not working', 'bottom', true, 1000);
                        // Error
                    }, function (progress) {
                        $ionicLoading.hide();
                        console.log(progress);
                        // constant progress updates
                    });

        } catch (e) {
            console.log('File Transfer Plugin will work on device');
            console.log(e);
        }
    };
    $scope.deleteUser = function (user_id, name, index) {
        alert(name);
        if ($scope.is_owner == "0")
        {
            ionicToast.show("you do not have permission to delete users", 'bottom', true, 1000);
        } else if (user_id == $localStorage.user.id)
        {
            ionicToast.show("owner cannot be deleted", 'bottom', true, 1000);
        } else {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Delete Flix',
                template: 'Are you sure you want to delete '+name+'?'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    $ionicLoading.hide();
                    $http({
                        url: $rootScope.apiUrl + "API/projects/delete_user",
                        method: 'POST',
                        data: {
                            project_id: $scope.project_id,
                            user_id: user_id
                        },
                        headers: {
                            'Access-Token': $localStorage.user.token,
                            'Email': $localStorage.user.email
                        }
                    }).success(function (response) {
                        $ionicLoading.hide();
                        if (response.status == '1') {
                            $scope.data.userList.splice(index, 1);
                            ionicToast.show("you have deleted " + name, 'bottom', true, 1000);
                        } else {
                            ionicToast.show(response.message, 'bottom', true, 1000);

                        }
                    }).error(function (erorr) {
                        $ionicLoading.hide();
                        ionicToast.show("API is not working", 'bottom', true, 1000);
                    });
                } else {
                    console.log('You are not sure');
                }
            });

        }
    };
});
    
