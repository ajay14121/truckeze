angular.module('truckeze.controllers').controller('projectHistoryCtrl', function($scope,$timeout,$localStorage,$stateParams,$interval, $http, $state, $ionicPopup, $rootScope, $ionicLoading,$ionicScrollDelegate,ionicToast) {
    $scope.$on('$ionicView.enter', function() {
         $scope.loadProjects();
    });
    $scope.loadProjects=function(){
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/Projects/list",
            method: 'POST',
            data:{
                is_history:1
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            if (response.status == '1') {
                $scope.project_list =  response.data;
            } else {
                $ionicPopup.alert({
                    title: "Failed",
                    template: response.message
                });
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Failed',
                template: 'API is not working'
            });
        });
    };
    $scope.projectDetail=function(id,ownership){
        $state.go("home",{project_id:id,is_owner:ownership});
    };
});
    
