angular.module('truckeze.controllers').controller('mainCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading,video) {
    video.addSource('mp4', 'http://clips.vorwaerts-gmbh.de/VfE_html5.mp4');
    $scope.data = {};
    $scope.data.first_name = '';
    $scope.data.last_name = '';
    $scope.data.email = '';
    $scope.data.password = '';
    $scope.signup = function() {
        console.log($scope.data);
        if ($scope.data.first_name == '') {
            $ionicPopup.alert({
                title: 'Empty Field',
                template: 'First name is mandatory'
            });
        }else if (!$rootScope.name.test($scope.data.first_name)) {
            $ionicPopup.alert({
                title: 'Invalid First Name',
                template: 'Only characters are allowed'
            });
        }else if ($scope.data.last_name === '') {
            $ionicPopup.alert({
                title: 'Empty Field',
                template: 'Last name is mandatory'
            });
        } else if (!$rootScope.name.test($scope.data.last_name)) {
            $ionicPopup.alert({
                title: 'Invalid Last Name',
                template: 'Only characters are allowed'
            });
        }else if ($scope.data.email === '') {
            $ionicPopup.alert({
                title: 'Empty Field',
                template: 'Email is mandatory'
            });
        } else if (!$rootScope.email_filter.test($scope.data.email)) {
            $ionicPopup.alert({
                title: 'Invalid Email',
                template: 'Your email id is not valid'
            });
        } else if ($scope.data.password === '') {
            $ionicPopup.alert({
                title: 'Empty Field',
                template: 'Password is mandatory'
            });
        } else {
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/register",
                method: 'POST',
                data: {
                    email: $scope.data.email,
                    password: $scope.data.password,
                    first_name: $scope.data.first_name,
                    last_name: $scope.data.last_name,
                }
            }).success(function(response) {
                $ionicLoading.hide();
                if (response.status) {
                    $ionicPopup.alert({
                        title: "Registration Request Sent",
                        template: "Please check your email for verification code"
                    }).then(function(res) {
                        console.log(res);
                        $location.path("/otp");
                    });
                } 
                else {
                    $ionicPopup.alert({
                        title: "Registration Failed",
                        template: response.message
                    });
                }
            }).error(function(erorr) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Signup Failed',
                    template: 'API is not working'
                });
            });
        }
    };
});
    
