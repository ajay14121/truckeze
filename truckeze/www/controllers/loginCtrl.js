angular.module('truckeze.controllers').controller('loginCtrl', function ($scope, $http, $location,$interval, $state, $ionicPopup, $rootScope, $ionicLoading, $localStorage, $cordovaFacebook,$ionicHistory) {
    $scope.data = {};
  $scope.countUnread = function () {
         var i=0;
                       $rootScope.badgeInterval=$interval(function () {
                           
                        console.log("counting in progress"+i);
                        i++;
                        $http({
                            url: $rootScope.apiUrl + "API/user/count",
                            method: 'POST',
                            headers: {
                                'Access-Token': $localStorage.user.token,
                                'Email': $localStorage.user.email
                            }
                        }).success(function (response) {
                            if (response.status == '1') {
                                console.log(response.data);
                                $rootScope.notificationCount=response.data.notification_count;
                                $rootScope.messageCount=response.data.msg_count;
                                
                            } else {
                                console.log("failed");
                            }
                        }).error(function (erorr) {
                            console.log("api not working");
                        });
                    }, 2000);
                    
                };
                
    $scope.login = function() {
        if ($scope.data.email == '' || $scope.data.password == '') {
            $ionicPopup.alert({
                title: 'Empty field',
                template: 'Email and password are mandatory'
            });
        } else if (!$rootScope.email_filter.test($scope.data.email)) {
            $ionicPopup.alert({
                title: 'Invalid Email',
                template: 'Please enter valid email'
            });
        } else {
            console.log($scope.data.email);
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/login",
                method: 'post',
                data: {
                    email: $scope.data.email,
                    password: $scope.data.password,
                    uuid: $rootScope.uuid,
                    platform: $rootScope.platform
                }
            }).success(function(response) {
                $ionicLoading.hide();
                if(response.status == 1) {
                    $localStorage.user = response.data;
                    $rootScope.user=$localStorage.user;
                    $scope.countUnread();
                    if (response.data.status == 'pending_approval') {
                        $state.go("otp");
                    } else if (response.data.status == 'active') {
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go("home", null, {reload: true});
                    } 
                } else {
                    $ionicPopup.alert({
                        title: 'Login Failed',
                        template: response.message
                    });
                }
            }).error(function(error) {
                $ionicLoading.hide();
                console.log(error);
                $ionicPopup.alert({
                    title: 'Api Error',
                    template: 'API is not working'
                });
            });

        }
    };
});
    
