angular.module('truckeze.controllers').controller('messagesCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading,$ionicScrollDelegate,$localStorage) {
    $scope.data = {};
    $scope.messagePage = function(project_id,project_name){
        $location.path("/message/"+project_id+"/"+project_name);
    };
    $scope.loadProjects = function(){
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/Projects/list",
            method: 'POST',
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            if (response.status == '1') {
                $scope.projects =  response.data;
            } else {
                $ionicPopup.alert({
                    title: "Failed",
                    template: response.message
                });
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Failed',
                template: 'API is not working'
            });
        });
    }
    $scope.loadProjects();
    
    
});


