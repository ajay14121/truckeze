// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('truckeze', ['ionic', 'truckeze.controllers', 'ion-datetime-picker', 'ngCordova', 'ngStorage','ionic-toast'])
        .run(function ($ionicPlatform, $rootScope, $localStorage, $cordovaDevice,$ionicHistory) {
            $ionicPlatform.ready(function () {
                $rootScope.apiUrl = "http://10.10.10.83/projects/bitbucket/truckeze/truckeze_api/index.php/";
  //              $rootScope.apiUrl = "http://giftsoninternet.com/all/truckeze/index.php/";
                $rootScope.build = "test"; // test or live
                
                // Set uuid & platform work only on device
                try {
                    $rootScope.uuid = $cordovaDevice.getUUID();
                    $rootScope.platform = $cordovaDevice.getPlatform();
                } catch (e) {
                    $rootScope.uuid = '';
                    $rootScope.platform = 'browser';
                }
                // Redirect on previous page
                $rootScope.myGoBack = function() {
                    $ionicHistory.goBack();
                };

                // Offline userLogin Count

                $rootScope.email_filter = /^[a-zA-Z0-9._]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                $rootScope.name= /^[a-zA-Z\s]*$/;
                $rootScope.digit= /^[0-9]+$/;
                $rootScope.item_added= /^[a-zA-Z0-9 ]*$/;


                if (window.cordova && window.cordova.plugins.Keyboard) {
                    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                    // for form inputs)
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                    // Don't remove this line unless you know what you are doing. It stops the viewport
                    // from snapping when text inputs are focused. Ionic handles this internally for
                    // a much nicer keyboard experience.
                    cordova.plugins.Keyboard.disableScroll(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }

            });
        })
        .config(function ($stateProvider, $urlRouterProvider, $httpProvider, $localStorageProvider,$ionicConfigProvider) {
            $ionicConfigProvider.tabs.position('bottom');
            $stateProvider
                    .state('app', {
                        url: '/app',
                        abstract: true,
                        templateUrl: 'templates/menu.html',
                        controller: 'menuCtrl'
                    })
                    .state('login', {
                        url: '/login',
                        templateUrl: 'templates/login.html',
                        controller: 'loginCtrl'

                    })
                    .state('setting', {
                        url: '/setting',
                        templateUrl: 'templates/setting.html',
                        controller: 'settingCtrl'
                    })
                    .state('main', {
                        url: '/main',
                        templateUrl: 'templates/main.html',
                        controller: 'mainCtrl'
                    })
                    .state('home', {
                        url: '/home/:project_id',
                        templateUrl: 'templates/home.html',
                        controller: 'homeCtrl',
                        cache:false
                      
                    })
                     .state('invitations', {
                        url: '/invitations/:project_id',
                        templateUrl: 'templates/invitation.html',
                        controller: 'invitationCtrl',
                        cache:false
                    })
                    .state('invites', {
                        url: '/invites',
                        templateUrl: 'templates/invites.html',
                        controller: 'invitesCtrl',
                        cache:false
                    })
                    .state('my-profile', {
                        url: '/my-profile',
                        templateUrl: 'templates/my-profile.html',
                        controller: 'myProfileCtrl',
                        cache:false
                      
                    })
                    .state('forget_password', {
                        url: '/forget_password',
                        templateUrl: 'templates/forget_password.html',
                        controller: 'forgetPasswordCtrl',
                    })
                    .state('change_password', {
                        url: '/change_password',
                        templateUrl: 'templates/change_password.html',
                        controller: 'changePasswordCtrl'
                    })
                    .state('signup', {
                        url: '/signup',
                        templateUrl: 'templates/signup.html',
                        controller: 'signupCtrl'
                    })
                    .state('otp', {
                        url: '/otp',
                        templateUrl: 'templates/otp.html',
                        controller: 'otpCtrl'
                    })
                    .state('search', {
                        url: '/search',
                        templateUrl: 'templates/search.html',
                        controller: 'searchCtrl'
                    })
                    .state('notifications', {
                        url: '/notifications',
                        templateUrl: 'templates/notifications.html',
                        controller: 'notificationsCtrl',
                        cache:false
                    })
                    .state('messages', {
                        url: '/messages',
                        templateUrl: 'templates/messages.html',
                        controller: 'messagesCtrl',
                        cache:false
                    })
                    .state('projects', {
                        url: '/projects',
                        templateUrl: 'templates/projects.html',
                        controller: 'projectsCtrl',
                        cache:false
                    })
                    .state('project_history', {
                        url: '/project_history',
                        templateUrl: 'templates/project_history.html',
                        controller: 'projectHistoryCtrl',
                        cache:false
                    })
                    .state('message', {
                        url: '/message/:project_id/:project_name',
                        templateUrl: 'templates/message.html',
                        controller: 'messageCtrl',
                        cache:false
                    })
                    ;

            // If user is logged in then redirect on user record page        
            if ($localStorageProvider.$get().user == undefined) {
                $urlRouterProvider.otherwise("/main");
            } else {
                $urlRouterProvider.otherwise("/home/");
            }
        });
