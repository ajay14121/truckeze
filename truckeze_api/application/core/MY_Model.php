<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @description	All common functions for models can be defined here and can be used in complete application.
 * 
 * @author	Hardeep <Hardeep.intersoft@gmail.com>
 * @date	12 June 2015
 * */
class MY_Model extends CI_Model {

    public $table = '';
    public $validate = array();

    /**
     * @Name : validates()
     * @Purpose : To check the validations of the form and return true if validations verified.
     * @Call from : Can be called from any controller.
     * @Functionality : Load form_validation library.
     *                  Set rules
     *                  Verify the validations 
     * @Receiver params : $data array() contains list of fields and values to validate
     * @Return params : returns true or false 
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 03 July 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 03 July 2015
     */
    public function validates($data = array()) {
        $this->load->library('form_validation');
        if (count($data) == 0)
            $data = $this->validate;
        foreach ($data as $validate)
            if (isset($validate['message']))
                $this->form_validation->set_rules($validate['field'], $validate['label'], $validate['rules'], $validate['message']);
            else
                $this->form_validation->set_rules($validate['field'], $validate['label'], $validate['rules']);
        return $this->form_validation->run();
    }

    /**
     * @Name : fetch_records()
     * @Purpose : To fetch the records from database table for pagination and listing.
     * @Call from : Can be called from any model and controller.
     * @Functionality : Simply gets the records from database tables with specified limit, conditions and starting point.
     * @Receiver params : $limit, $start, $conditions
     * @Return params : Return array of records from table 
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     */
    public function fetch_records($limit = null, $start = null, $conditions = array(), $col = NULL, $orderBy = "DESC") {

        if (!is_null($limit) && !is_null($start)) {
            $this->db->limit($limit, $start);
        }
        if (!is_null($col)) {
            $this->db->order_by($col, $orderBy);
        }
        if (!empty($conditions)) {
            foreach ($conditions as $condition) {
                $this->db->where($condition);
            }
        }
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    /**
     * @Name : count_results()
     * @Purpose : To count to number of rows in the database.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Simply count the results using predefined count_all_results() function.
     * @Receiver params : No parameters passed
     * @Return params : Returns the count of rows.
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     */
    function count_results($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db->where($where);
        }
        $getData = $this->db->count_all_results($this->table);
        return $getData;
    }

    /**
     * @Name : save()
     * @Purpose : To save the records in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Saves the entry in specified database table.
     * @Receiver params : $data array of values to save in database.
     * @Return params : return last insert id if record is saved else return false.
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 03 July 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 03 July 2015
     */
    function save($data = array()) {
        if (isset($data['password'])) {
            $data['password'] = $this->hash_this($data['password']);
        }
        if ($this->db->insert($this->table, $data))
            return $this->db->insert_id();
        else
            return false;
    }

    /**
     * @Name : update()
     * @Purpose : To save the records in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Saves the entry in specified database table.
     * @Receiver params : $data array of values to save in database.
     * @Return params : return true if record is saved else return false.
     * @Created : Sonia <sonia.intersoft@gmail.com> on 03 July 2015
     * @Modified : Sonia <sonia.intersoft@gmail.com>on 03 July 2015
     */
    function update($data = array(), $where = array()) {
        if (isset($data['password'])) {
            $data['password'] = $this->hash_this($data['password']);
        }
        $query = $this->db->update($this->table, $data, $where);
        return $query;
    }

    /**
     * @Name : delete()
     * @Purpose : To delete the record in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : delete the entry from the database table.
     * @Receiver params : array of values to filter the record to be deleted
     * @Return params : return true if record is deleted else return false.
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 03 July 2015
     * @Modified :
     */
    function delete($where = array()) {
        if ($this->db->delete($this->table, $where))
            return true;
        else
            return false;
    }

    function select_record($where, $table = NULL) {
        if (isset($where['password'])) {
            $where['password'] = $this->hash_this($where['password']);
        }
        $this->db->Select('*');
        $table = is_null($table) || $table == "" ? $this->table : $table;
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else
            return false;
    }

    /**
     * @Name : hash_this()
     * @Purpose : Convert the hash value of the value passed as parameter.
     * @Call from : Authentication.php controller file.
     * @Functionality : Convert the value hash using sha1 algorithm functions.
     * @Receiver params : $value_to_hash value to be converted.
     * @Return params : return converted hash value.
     * @Created : Sonia <sonia.intersoft@gmail.com> on 03 July 2015
     * @Modified : Sonia <sonia.intersoft@gmail.com>on 03 July 2015
     */
    function hash_this($value_to_hash) {
        $security_salt = 'h!u@n#z$o%n^i&a*n';
        $hash_result = sha1(sha1($value_to_hash . $security_salt));
        return $hash_result;
    }

    function getLatLang($address = '') {
        $response = json_decode(file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . urlencode($address) . "&sensor=false"));
        if ($response && isset($response->results[0])) {
            $res['lat'] = $response->results[0]->geometry->location->lat;
            $res['long'] = $response->results[0]->geometry->location->lng;
        } else {
            $res['lat'] = '';
            $res['long'] = '';
        }
        return $res;
    }
    /**
     * @Name : generate_key()
     * @Purpose : To generate unique 10 digit code.
     * @Call from : Can be called from any controller file.
     * @Functionality : to generate the Activation Key
     * @Receiver params : One optional Length parameter Recieved
     * @Return params : Return the code generated
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 4 July 2015
     * @Modified :
     */
    public function generate_random_string($length = 10) {
       
        $this->load->model($this->model);
        
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        
        return $key;
    }

}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */