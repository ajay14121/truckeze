<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Name:  MY_Controller
 *
 */
class MY_Controller extends CI_Controller {

    public $layout = 'default';
    public $data = array();

    public function __construct() {
        parent::__construct();

        if ($this->session->has_userdata('agent_id') && !$this->input->is_ajax_request() && $this->session->userdata('role') == 'agent') {
            $this->layout = 'agent';
        }

        $current_uri = $this->router->fetch_class() . "/" . $this->router->fetch_method();
        if (!in_array($current_uri, $this->config->config['before_login_pages'])) {
//        if ($current_uri != 'authentication/index' && $current_uri != 'authentication/login' && $current_uri != 'authentication/agent_login') {
            $this->load->theme($this->layout);
            $check_login = false;
            $check_login = USER_TYPE == 'admin'?$this->isLogin() : $this->isAgentLogin();
            if (!$check_login) {
                if ($current_uri != 'information/privacy_policy' && $current_uri != 'information/terms_conditions'/* && $current_uri != 'home/test_notification'*/) {
                    redirect('authentication/index');
                }
            }
        }
    }

    /**
     * @Name : pagination_links()
     * @Purpose : Create pagination links with for the view.
     * @Call from : Can be called from controller functions so that we can set the returned values to the view.
     * @Functionality : Loads pagination library, intialize the pagination class and create links and return the required values.
     * @Receiver params : $total_rows, $total_rows_per_page, $page
     * @Return params : Return array('results', links)
     */
    public function pagination_link($model, $orderBy = "DESC", $conditions = NULL, $orderByCol = NULL) {
        $last = $this->uri->total_segments();
        $record_num = $this->uri->segment($last);
        $data = array();
        $this->load->library("pagination");
        $this->load->Model($model);
        $config['per_page'] = ROWS_PER_PAGE;

        $config['base_url'] = site_url($this->uri->segment(1) . "/" . $this->uri->segment(2));
        $total_rows = $this->{$model}->count_results($conditions);
        $config['total_rows'] = $total_rows;
        $config['first_tag_open'] = $config['last_tag_open'] = $config['next_tag_open'] = $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close'] = $config['next_tag_close'] = $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $config['reuse_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) && ($this->uri->segment(2) != 'user_edit') ? $this->uri->segment(3) : 0;

        $orderByCol = $orderByCol == NULL ? $this->{$model}->primary_key : $orderByCol;
        if (count($conditions) > 0 || !empty($conditions)) {
            $data["results"] = $this->{$model}->fetch_records($config["per_page"], $page, $conditions, $orderByCol, $orderBy);
        } else
            $data["results"] = $this->{$model}->fetch_records($config["per_page"], $page, $conditions = array(), $orderByCol, $orderBy);


        $data["links"] = $this->pagination->create_links();
        if (!empty($data["links"])) {
            $data["links"] = '<ul class="pagination bootpag"><li class="prev" data-lp="2">' . $data["links"] . '</li></ul>';
        }

        if ($page > 0 && !$data["results"]) {
            $page = $page - $config['per_page'];
            redirect($this->uri->segment(1) . "/" . $this->uri->segment(2));
        }
        $data['total_rows'] = $total_rows;
        $data['page'] = $page;
        return $data;
    }

    /**
     * @Name : fetch_single_record()
     * @Purpose : To fetch single record from the table.
     * @Call from : Can be called from any controller file.
     * @Functionality : to fetch the record from the database
     * @Receiver params : array of parameters to filter the records to be fetched
     * @Return params : Return true or false.
     */
    public function fetch_single_record($model, $where = array(), $table = NULL) {
        $this->load->model($model);
        $return = $this->{$model}->select_record($where, $table);
        return $return;
    }

    /**
     * @Name : camelize()
     * @Purpose : To convert the value in camel case e.g camel_case => CamelCase.
     * @Call from : Can be called from any controller file.
     * @Functionality : Replace the _ and capitalise the charecters after underscore.
     * @Receiver params : $word to change thhe case
     * @Return params : Return the converted value.
     */
    function camelize($word) {
        return preg_replace('/(^|_)([a-z])/e', 'strtoupper("\\2")', $word);
    }

    /**
     * @Name : decamelize()
     * @Purpose : To convert the value from camel case e.g CamelCase => camel_case.
     * @Call from : Can be called from any controller file.
     * @Functionality : Replace the _ and capitalise the charecters after underscore.
     * @Receiver params : $word to change thhe case
     * @Return params : Return the converted value.

     */
    function decamelize($word) {
        return preg_replace(
                '/(^|[a-z])([A-Z])/e', 'strtolower(strlen("\\1") ? "\\1_\\2" : "\\2")', $word
        );
    }

    /**
     * @Name : format_date()
     * @Purpose : To change the format of the date.
     * @Call from : Can be called from any controller file.
     * @Functionality : change the format of the date according to the date specified.
     * @Receiver params : date string, Format of the date
     * @Return params : Return the converted value.
     */
    function format_date($dateString, $format) {
        if (is_null($dateString) || empty($dateString) || $dateString == "0000-00-00 00:00:00") {
            return "";
        }

        $date = date_create($dateString);
        return date_format($date, $format);
    }

    /**
     * @Name : isLogin()
     * @Purpose : To check user is logged in or not.
     * @Call from : Can be called from any controller file.
     * @Functionality : if user has already logged in or not.
     * @Receiver params : empty
     * @Return params : Return true or false.
     */
    public function isLogin() {
        if ($this->session->has_userdata('id') && $this->session->userdata('role') == 'admin') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @Name : isLogin()
     * @Purpose : To check user is logged in or not.
     * @Call from : Can be called from any controller file.
     * @Functionality : if user has already logged in or not.
     * @Receiver params : empty
     * @Return params : Return true or false.
     */
    public function isAgentLogin() {
        if ($this->session->has_userdata('agent_id') && $this->session->userdata('role') == 'agent') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @Name : delete_record()
     * @Purpose : To delete user.
     * @Call from : Can be called from any controller file.
     * @Functionality : to delete the record from the database
     * @Receiver params : UserId
     * @Return params : Return true or false.
     */
    public function delete_record($where = array()) {
        $this->load->model($this->model);
        return $this->{$this->model}->delete($where);
    }

    public function check_existance_inTables($where = array(), $fk_tables = array()) {
        $this->load->model($this->model);
        if (count($fk_tables) > 0 && count($where) > 0) {
            foreach ($fk_tables as $table) {
                $this->load->model($this->model);
                $data = $this->{$this->model}->select_record($where, $table);
                if ($data) {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    //send iphone push notification
    public function IphonePushNotification($deviceToken = '', $title = '', $message = '', $build = '') {
        require_once FCPATH . 'application/libraries/PushDemo/simplepush.php';
        $result = sendPushMessage($deviceToken, $title, $message, $build);
        return $result;
    }

    public function sendNotification($user_details = array(), $property_detail = array()) {
        if (!empty($user_details)) {
            $title = 'Delete notification';
            $message = 'Property at ' . $property_detail['building_street_1'] . ' ' . $property_detail['property_address_2'] . ' address is deleted.';

            foreach ($user_details as $user) {
                $result = FALSE;
                if ($user['device_token']) {
                    $result = $this->IphonePushNotification($user['device_token'], $title, $message, $user['build']);
                    $this->db->insert('notifications', array('device_token' => $user['device_token'], 'status' => $result, 'message' => $message, 'build_type' => $user['build']));
                }
            }
        }
        return TRUE;
    }

    /**
     * @Name : unique_key()
     * @Purpose : To generate a unique Id
     * @Call from : Can be called from any controller file.
     * @Functionality : to generate a unique id
     * @Receiver params : No parameters recieved
     * @Return params : Return a unique Key.
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on September 14 2015
     * @Modified :
     */
    public function unique_key()
    {
        list($microseconds, $seconds) = explode(' ', microtime());
        $unique_id = md5($seconds + $microseconds . getmypid());
        return $unique_id;
    }

}
