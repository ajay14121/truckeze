<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require(APPPATH . 'libraries/REST_Controller.php');

class Message extends REST_Controller {

    var $data = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('message_model');
        $this->load->model('projects_model');
        $this->load->library('PhpMailer');
        $this->data = array();
        $this->mail = new CI_PHPMailer();
        if (IsSMTP) {
            $this->mail->IsSMTP(); // send via SMTP
        }

        $this->mail->Host = smtp_Host; // SMTP servers
        $this->mail->SMTPAuth = smtp_SMTPAuth; // turn on SMTP authentication
        $this->mail->Username = smtp_Username; // SMTP username 
        $this->mail->Password = smtp_Password; // SMTP password
        $this->mail->SMTPDebug = smtp_SMTPDebug; //Enable SMTP debugging
        $this->mail->Port = smtp_Port; //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $this->mail->SMTPSecure = smtp_SMTPSecure; // Set the encryption system to use - ssl (deprecated) or tls
        $this->mail->Debugoutput = smtp_Debugoutput; //Ask for HTML-friendly debug output
    }

    function send_post() {
        $status = NULL;
        $message = NULL;
        $this->api_manager->handle_request();
        $this->form_validation->set_rules('send_to', 'Send To', 'trim|required');
        $this->form_validation->set_rules('type', 'Type', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => '0',
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
            $this->response(array(
                'status' => '0',
                'message' => explode("\n", validation_errors())
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        } else {
            $data['type'] = $this->api_manager->parse_request('type');
            $data_msg['msg'] = $this->api_manager->parse_request('message');
            $data['send_from'] = $user_details['user_id'];
            $data['to'] = $this->api_manager->parse_request('send_to');
            
            $data['msg_id'] = $this->message_model->save($data_msg);
            if ($data['msg_id']) {
                if ($data['type'] = 'group') {
                    $data1 = array('project_id' => $data['to']);
                    $result = $this->projects_model->fetch_user_project_data($data1);
                    if ($result) {
                        $i = '1';
                        foreach ($result as $res) {
                            $data2[$i] = array('msg_id' => $data['msg_id'], 'to' => $res['user_id'], 'type' => 'group', 'group_id' => $data['to'], 'from' => $user_details['user_id']);
                            $i++;
                        }
                        if ($this->message_model->batch_query($data2)) {
                            $message = 'message send successfully';
                            $status = '1';
                        }
                    } else {
                        $message = 'invalid request';
                        $status = '0';
                    }
                } else {
                        $data2 = array('msg_id' => $data['msg_id'], 'to' => $data['to'], 'type' => 'group', 'from' => $user_details['user_id']);
                    if ($this->message_model->insert_message_recipient($data2)) {
                        $message = 'Message send successfully';
                        $status = '1';
                    } else {
                        $message = 'invalid request';
                        $status = '0';
                    }
                }
            } else {
                $message = 'message not sent';
                $status = '0';
            }
            $this->response(array(
                'status' => $status,
                'message' => $message,
            ), REST_Controller::HTTP_OK);
           
        }
    }

    function list_post() {
        $this->api_manager->handle_request();
        $this->form_validation->set_rules('project_id', 'Project Id', 'trim|required');
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => '0',
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
            $this->response(array(
                'status' => '0',
                'message' => explode("\n", validation_errors())
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        } else {
            $project_id = $this->api_manager->parse_request('project_id');
            $data = array('project_id' => $project_id, 'type' => 'group');
            $result = $this->message_model->select_message_recipient($data);
          
	    
            if ($result) {
	    	foreach($result as $key =>$res){
               $result[$key]['profile_image'] = base_url('uploads/profile_images/' . $user_details['user_id']."/".$res['profile_image']);
            }
	    
                $message = 'get message successfully';
                $status = '1';
                $response = array_values($result);
                $this->response(array(
                    'status' => $status,
                    'message' => $message,
                    'data' => $response
                        ), REST_Controller::HTTP_OK);
            } else {
                $this->response(array(
                    'status' => '0',
                    'message' => 'no messages',
                        ), REST_Controller::HTTP_OK);
            }
        }
    }

    function update_post() {
        $this->api_manager->handle_request();
        $this->form_validation->set_rules('msg_id', 'msg_id', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => '0',
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
            $this->response(array(
                'status' => '0',
                'message' => explode("\n", validation_errors())
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        } else {
            $status = $this->api_manager->parse_request('status');
            $msg = $this->api_manager->parse_request('msg_id');
            $msg_id = explode(',', $msg);
            foreach($msg_id as $m_id){
            $where = array('msg_id' => $m_id, 'is_read' => '0','to'=>$user_details['user_id']);
            $data = array('is_read' => $status);
            if ($this->message_model->select_record($where, 'message_recipients')) {
                if ($this->message_model->update_message_recipiente($data, $where)) {
                    $message = 'update successfully';
                    $status  = '1';
                }
                else {
                   $message = 'not update';
                    $status  = '0';
                }
            } else {
                $message = 'invalid request';
                $status  = '0';
            }
        }
        }
        $this->response(array(
                    'status' => $status,
                    'message' => $message,
                        ), REST_Controller::HTTP_OK);
    }

    function delete_post() {
       $this->api_manager->handle_request();
        $this->form_validation->set_rules('msg_id', 'msg_id', 'trim|required');
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => '0',
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
            $this->response(array(
                'status' => '0',
                'message' => explode("\n", validation_errors())
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        } else {
            $msg_id = $this->api_manager->parse_request('msg_id');
            $where = array('msg_id' => $msg_id,);
            if ($this->message_model->select_record($where, 'message')) {
                if ($this->message_model->delete($where)) {
                    $this->response(array(
                        'status' => '1',
                        'message' => 'delete successfully',
                            ), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array(
                        'status' => '0',
                        'message' => 'not deleted',
                            ), REST_Controller::HTTP_OK);
                }
            } else {
                $this->response(array(
                    'status' => '0',
                    'message' => 'invalid request',
                        ), REST_Controller::HTTP_OK);
            }
        }
    }

}
