<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require(APPPATH . 'libraries/REST_Controller.php');

class Projects extends REST_Controller {

    var $data = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('api_manager');
        $this->load->library('form_validation');
        $this->load->library('custom_file');
        $this->load->library('REST_Controller');
        $this->load->model('projects_model');
        $this->load->model('users_model');
        $this->load->model('notification_model');
        $this->load->library('PhpMailer');
        $this->data = array();
        $this->mail = new CI_PHPMailer();
        if (IsSMTP) {
            $this->mail->IsSMTP(); // send via SMTP
        }

        $this->mail->Host = smtp_Host; // SMTP servers
        $this->mail->SMTPAuth = smtp_SMTPAuth; // turn on SMTP authentication
        $this->mail->Username = smtp_Username; // SMTP username 
        $this->mail->Password = smtp_Password; // SMTP password
        $this->mail->SMTPDebug = smtp_SMTPDebug; //Enable SMTP debugging
        $this->mail->Port = smtp_Port; //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $this->mail->SMTPSecure = smtp_SMTPSecure; // Set the encryption system to use - ssl (deprecated) or tls
        $this->mail->Debugoutput = smtp_Debugoutput; //Ask for HTML-friendly debug output
    }

    private function sendMail($parms) {

        $this->mail->ContentType = isset($parms['ContentType']) ? $parms['ContentType'] : 'text/html';
        $this->mail->From = 'info@abc.com';
        $this->mail->FromName = 'abc';
        if (strpos($parms['To'], ',') !== false || strpos($parms['To'], ';') !== false) {
            $parms['To'] = explode(",", $parms['To']);
        }
        if (is_array($parms['To'])) {
            foreach ($parms['To'] as $email) {
                $this->mail->AddAddress($email);
            }
        } else {
            $this->mail->AddAddress($parms['To']);
        }
        if (isset($parms['From'])) {
            $this->mail->AddReplyTo($parms['From'], $this->mail->FromName);
        }
        $this->mail->Subject = isset($parms['Subject']) ? $parms['Subject'] : 'abc';
        $this->mail->msgHTML(isset($parms['Body']) ? $parms['Body'] : '');
        if ($result = $this->mail->Send())
            return $result;
        else
            return FALSE;
    }

    function create_post() {
        $this->api_manager->handle_request();
        $this->form_validation->set_rules('project_name', 'Project Name', 'trim|required');
        $this->form_validation->set_rules('deadline', 'Deadline', 'trim|required', 'regex_match[(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})]');
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => '0',
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
            $this->response(array(
                'status' => '0',
                'message' => explode("\n", validation_errors())
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        } else {

            $data['project_name'] = $this->api_manager->parse_request('project_name');
            $deadline = $this->api_manager->parse_request('deadline');
            $data['deadline'] = date("Y-m-d H:i:s",strtotime($deadline));
            //$result = $this->users_model->check_project($user_details['user_id'],$data['project_name']);
//           echo $this->db->last_query();
//           exit;
            //if(!$result){
//               $this->response(array(
//                'status' => '0',
//                'message' => 'invalid request',
//                 ), REST_Controller::HTTP_OK);
            //}else{
            $project_id = $this->projects_model->save($data);
            $project_user['user_id'] = $user_details['user_id'];
            $project_user['project_id'] = $project_id;
            $project_user['is_owner'] = '1';
            $project_user['is_accept'] = '1';
            $this->projects_model->project_users($project_user);
            
            $user_info['project_id'] = $project_id;
            $this->response(array(
                'status' => '1',
                'message' => 'Project created successfully',
                'data' => $user_info
                    ), REST_Controller::HTTP_OK);
            // }
        }
    }

    function invitation_post() {
        $this->api_manager->handle_request();
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('project_id', 'Project Id', 'trim|required');
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => '0',
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
            $this->response(array(
                'status' => '0',
                'message' => explode("\n", validation_errors())
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        } else {
            $data['project_id'] = $this->api_manager->parse_request('project_id');
            $data['email'] = $this->api_manager->parse_request('email');
            $data['name'] = $this->api_manager->parse_request('name');
            $where['project_id'] = $data['project_id'];
            $where['user_id'] = $user_details['user_id'];
            $where['is_owner'] = '1';
            $fetch_project = array('project_id' => $data['project_id']);
            $result_project_name = $this->projects_model->select_record($fetch_project, 'projects');
            $result_project = $this->projects_model->select_record($where, 'project_users');
            if (!$result_project) {
                $this->response(array(
                    'status' => '0',
                    'message' => 'invalid request',
                        ), REST_Controller::HTTP_OK);
            } else {
                $name_array = explode(",",$data['name']);
                $email_array = explode(",", $data['email']);
                foreach ($email_array as $key=>$email) {
                    $email1 = array('email' => $email);
                    $result = $this->projects_model->select_record($email1, 'users');
                    if (!$result) {
                        $data['name'] = $name_array[$key];
                        $user['email'] = $email;
                        $user['first_name'] = $name_array[$key];
                        $user['is_searchable'] = '0';
                        $user_data['user_id'] = $this->users_model->save($user);
                    } else {
                        $data['name'] = $result['first_name'];
                        $user_data['user_id'] = $result['id'];
                        }
                        $data['project_name'] = $result_project_name['project_name'];
                        $mail_params['ContentType'] = "text/html";
                        $mail_params['To'] = $email;
                        $mail_params['Subject'] = 'invitation';
                        $mail_params['Body'] = $this->load->view('email_format/invitation_email', $data, true);
                        if ($this->sendMail($mail_params)) {
                            
                        } 
                    $user_data['project_id'] = $data['project_id'];
                    $insert_id = $this->projects_model->project_users($user_data);
                    if ($insert_id) {
                        $notify_users_detail = $this->users_model->getUserDevices($user_data['user_id']);
                        $notification_details = array();
                        $notification_details['title'] = 'Invitation';
                        $notification_details['message'] = 'Congratulations, '.$user_details['first_name'].' '.$user_details['last_name'].' has invited you for '. $result_project_name['project_name'] .' Flix';
                    foreach ($notify_users_detail as $notify_data) {
                        if ($notify_data['device_token']) {
                            $notification_details['device_tokens'][] = $notify_data['device_token'];
                            $notification_details['platform'][] = $notify_data['platform'];
                            $notification_details['build'][] = $notify_data['build'];
                        }
                    }
                        if ($notification_details['device_tokens']) {
                            $this->api_manager->sendNotification($notification_details);
                        }
                        $notification['come_from'] = $user_details['user_id'];
                        $notification['send_to'] = $user_data['user_id'];
                        $notification['notification'] = 'Congratulations, '.$user_details['first_name'].' '.$user_details['last_name'].' has invited you for '. $result_project_name['project_name'] .' Flix';
                        $noti_id = $this->notification_model->save($notification);
                        $message = 'Send invitation successfully';
                        $status = '1';
                    } else {
                        $status = '0';
                        $mesaage = 'invitation not send';
                    }
                }
                $this->response(array(
                    'status' => $status,
                    'message' => $message,
                        //'data' =>$response_data
                        ), REST_Controller::HTTP_OK);
            }
        }
    }

    function delete_project_post() {
        $this->api_manager->handle_request();
        $this->form_validation->set_rules('project_id', 'Project Id', 'trim|required');
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => '0',
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
            if (!$this->input->post()) {
                $this->response(array(
                    'status' => '0',
                    'message' => explode("\n", validation_errors())
                        ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                die();
            }
        } else {
            $data['project_id'] = $this->api_manager->parse_request('project_id');
            $where['project_id'] = $data['project_id'];
            $where['is_deleted'] = '1';
            $result_project = $this->projects_model->select_record($where, 'projects');
            if (!$result_project) {
                $this->response(array(
                    'status' => '0',
                    'message' => 'invalid request',
                        ), REST_Controller::HTTP_OK);
            } else {
                $project_name = $result_project['project_name'];
                $update['is_deleted'] = '0';
                $data1 = array('project_id' => $data['project_id']);
                $result = $this->projects_model->update($update, $data1);
                if (!$result) {
                    $status = '0';
                    $message = 'project is not deleted';
                } else {
                    $data['is_owner'] = '0';
                    $user_data = $this->projects_model->fetch_user_project_data($data);
                    foreach ($user_data as $users) {
                        $notification['come_from'] = $user_details['user_id'];
                        $notification['send_to'] = $users['user_id'];
                        $notification['notification'] = $project_name . "  " . 'project is deleted';
                        $noti_id = $this->notification_model->save($notification);
                        $message = 'Deletion invitation successfully send ';
                        $status = '1';
                    }
                }
                $this->response(array(
                    'status' => $status,
                    'message' => $message,
                        ), REST_Controller::HTTP_OK);
            }
        }
    }

    function delete_user_post() {
        $this->api_manager->handle_request();
        $this->form_validation->set_rules('project_id', 'Project Id', 'trim|required');
        $this->form_validation->set_rules('user_id', 'User Id', 'trim|required');
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => '0',
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
            $this->response(array(
                'status' => '0',
                'message' => explode("\n", validation_errors())
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        } else {
            $data['project_id'] = $this->api_manager->parse_request('project_id');
            $data['user_id'] = $this->api_manager->parse_request('user_id');
            $where = array('project_id' => $data['project_id']);
            $result_project = $this->projects_model->select_record($where, 'projects');
            if (!$result_project) {
                $this->response(array(
                    'status' => '0',
                    'message' => 'invalid request',
                        ), REST_Controller::HTTP_OK);
            } else {
                $project_name = $result_project['project_name'];
                $insert_id = $this->projects_model->select_record($data, 'project_users');
                if ($insert_id) {
                    $result = $this->projects_model->delete_users($data);
                    if ($result) {
                        $notification['come_from'] = $user_details['user_id'];
                        $notification['send_to'] = $data['user_id'];
                        $notification['notification'] = 'you have removed from' . " " . $project_name . ' ' . 'project';
                        $this->response(array(
                            'status' => '1',
                            'message' => 'user is  deleted successfully',
                                ), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array(
                            'status' => '0',
                            'message' => 'user is not deleted',
                                ), REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response(array(
                        'status' => '0',
                        'message' => 'invalid request',
                            ), REST_Controller::HTTP_OK);
                }
            }
        }
    }

    function update_users_post() {
        $this->api_manager->handle_request();
        $this->form_validation->set_rules('invitation_id', 'Invitation Id', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => '0',
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
            $this->response(array(
                'status' => '0',
                'message' => explode("\n", validation_errors())
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        } else {
            $insert_data = array();
            $data['id'] = $this->api_manager->parse_request('invitation_id');
            $data['status'] = $this->api_manager->parse_request('status');
            $data1['id'] = $data['id'];
            $data1['is_accept'] = '0';
            $data1['is_owner'] = '0';
            $res = $this->projects_model->select_record($data1, 'project_users');
            if (!$res) {
                $this->response(array(
                    'status' => '0',
                    'message' => 'invalid request',
                        ), REST_Controller::HTTP_OK);
            } else {
                if ($data['status'] == '1' || $data['status'] == '2') {
                    $insert_data['is_accept'] = $data['status'];
                    $result = $this->projects_model->update_project_user($insert_data, $data['id']);
                    if ($result) {
                        $noti = ($data['status'] == '1') ? 'accept invitation' : 'reject invitation';
                        $notification['come_from'] = $res['user_id'];
                        $notification['send_to'] = $user_details['user_id'];
                        $notification['notification'] = $noti;
                        $noti_id = $this->notification_model->save($notification);
                        $this->response(array(
                            'status' => '1',
                            'message' => 'Update successfully',
                                ), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array(
                            'status' => '0',
                            'message' => 'Not update',
                                ), REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response(array(
                        'status' => '0',
                        'message' => 'invalid request',
                            ), REST_Controller::HTTP_OK);
                }
            }
        }
    }

    function list_post() {
        $this->api_manager->handle_request();
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => '0',
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $is_owner = $this->api_manager->parse_request('is_owner');
        $is_history = $this->api_manager->parse_request('is_history');
        $where['user_id'] = $user_details['user_id'];
        $where['is_accept'] = '1';
        $where['is_deleted'] = '1';
        if (!empty($is_owner)) {
            $where['is_owner'] = $is_owner;
        } 
        if (!empty($is_history)){
           if($is_history == '1') {
           $where['deadline <='] = date("Y-m-d H:i:s");
           }else{
           $where['deadline >'] = date("Y-m-d H:i:s");
        }
        }
        $result = $this->projects_model->get_projects($where);
        if (!$result) {
            $this->response(array(
                'status' => '0',
                'message' => 'no record found',
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => '1',
                'message' => 'fetch projects listing successfully',
                'data' => $result,
            ), REST_Controller::HTTP_OK);
        }
    }

    function project_details_post() {
        $this->api_manager->handle_request();
        $this->form_validation->set_rules('project_id', 'Project Id', 'trim|required');
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => '0',
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
            $this->response(array(
                'status' => '0',
                'message' => explode("\n", validation_errors())
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        } else {
            $project_id = $this->api_manager->parse_request('project_id');
            $data = array('project_id'=>$project_id);
            $result = $this->projects_model->project_details($project_id);
            if ($result) {
                $result1['is_owner'] = 0;
                foreach ($result as $key => $res) {
                    $result1['project_name'] = $res['project_name'];
                    $result1['deadline'] = $res['deadline'];
                    $result1['invited_users'][$key]['name'] = $res['first_name'] . ' ' . $res['last_name'];
                    $result1['invited_users'][$key]['is_accept'] = $res['is_accept'];
                    $result1['invited_users'][$key]['video'] = base_url('uploads/videos/' .$project_id . '/' . $res['video']);
                    $result1['invited_users'][$key]['image'] = base_url('uploads/profile_images/' . $res['profile_image']);
                    $result1['invited_users'][$key]['is_owner'] = $res['is_owner'];
                    $result1['invited_users'][$key]['user_id'] = $res['user_id'];
                    if($user_details['user_id'] == $res['user_id']){
                       $result1['is_owner'] = $res['is_owner'];
                    }
                }
                $this->response(array(
                    'status' => '1',
                    'message' => 'fetch projects details successfully',
                    'data' => $result1,
                ), REST_Controller::HTTP_OK);
            } else {
                $this->response(array(
                    'status' => '1',
                    'message' => 'no record found',
                    'data' => $result,
                ), REST_Controller::HTTP_OK);
            }
        }
    }
    
    function invites_post() {
        $this->api_manager->handle_request();
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => '0',
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        
        $result = $this->projects_model->project_invites($user_details['user_id']);
        if ($result) {
            $this->response(array(
                'status' => '1',
                'message' => 'fetch invites successfully',
                'data' => $result,
            ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => '1',
                'message' => 'no record found',
                'data' => $result,
            ), REST_Controller::HTTP_OK);
        }
    }

}
