<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require(APPPATH . 'libraries/REST_Controller.php');

class Notification extends REST_Controller {

    var $data = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('api_manager');
        $this->load->library('form_validation');
        $this->load->library('custom_file');
        $this->load->library('REST_Controller');
        $this->load->model('notification_model');
        $this->load->library('PhpMailer');
        $this->data = array();
        $this->mail = new CI_PHPMailer();
        if (IsSMTP) {
            $this->mail->IsSMTP(); // send via SMTP
        }

        $this->mail->Host = smtp_Host; // SMTP servers
        $this->mail->SMTPAuth = smtp_SMTPAuth; // turn on SMTP authentication
        $this->mail->Username = smtp_Username; // SMTP username 
        $this->mail->Password = smtp_Password; // SMTP password
        $this->mail->SMTPDebug = smtp_SMTPDebug; //Enable SMTP debugging
        $this->mail->Port = smtp_Port; //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $this->mail->SMTPSecure = smtp_SMTPSecure; // Set the encryption system to use - ssl (deprecated) or tls
        $this->mail->Debugoutput = smtp_Debugoutput; //Ask for HTML-friendly debug output
    }

    function update_post() {
        $this->api_manager->handle_request();
        $this->form_validation->set_rules('notification_id', 'Notification Id', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => '0',
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
            $this->response(array(
                'status' => '0',
                'message' => explode("\n", validation_errors())
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        } else {
            $data['notification_id'] = $this->api_manager->parse_request('notification_id');
            $data['is_read'] = $this->api_manager->parse_request('status');
            $noti_id = explode(',', $data['notification_id']);
            foreach($noti_id as $n_id){
            $search = array('notification_id' => $n_id);
            $result = $this->notification_model->select_record($search, 'notifications');
            if ($data['is_read'] == '1') {
                if ($result) {
                    $update['is_read'] = $data['is_read'];
                    $data1 = array('notification_id' => $n_id);
                    $res = $this->notification_model->update($update, $data1);
                    if (!$res) {
                       $status = '0';
                       $message = 'not updated';
                       } else {
                         $status = '1';
                       $message = 'update successfully';
                    }
                } else {
                       $status = '1';
                       $message = 'no record foundf';
                }
            } 
            }
             $this->response(array(
                            'status' => $status,
                            'message' => $message,
                                ), REST_Controller::HTTP_OK);
        }
    }

    function list_post() {
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => '0',
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $data = array(array('send_to' => $user_details['user_id']));
        $result = $this->notification_model->fetch_records(null, null, $data, null);
        $i = '1';
        if (!$result) {
            $this->response(array(
                'status' => '1',
                'message' => 'no record found',
                'data' => '',
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => '1',
                'message' => 'fetch notification listing successfully',
                'data' => $result,
                    ), REST_Controller::HTTP_OK);
        }
    }

}
