<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require(APPPATH . 'libraries/REST_Controller.php');

class User extends REST_Controller {

    var $data = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('users_model');
         $this->load->model('projects_model');
         $this->load->model('message_model');
         $this->load->model('notification_model');
         $this->model = 'users_model';
        $this->load->library('PhpMailer');
        $this->data = array();
        $this->mail = new CI_PHPMailer();
        if (IsSMTP) {
            $this->mail->IsSMTP(); // send via SMTP
        }
        $this->mail->Host = smtp_Host; // SMTP servers
        $this->mail->SMTPAuth = smtp_SMTPAuth; // turn on SMTP authentication
        $this->mail->Username = smtp_Username; // SMTP username 
        $this->mail->Password = smtp_Password; // SMTP password
        $this->mail->SMTPDebug = smtp_SMTPDebug; //Enable SMTP debugging
        $this->mail->Port = smtp_Port; //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $this->mail->SMTPSecure = smtp_SMTPSecure; // Set the encryption system to use - ssl (deprecated) or tls
        $this->mail->Debugoutput = smtp_Debugoutput; //Ask for HTML-friendly debug output
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $this->api_manager->handle_request();
        $this->data['error']['message'] = 'Invalid Request';
        $this->data['error']['reason'] = 'invalid_endpoint';
        $this->api_manager->set_output($this->data);
    }

    /**
     * @name sendMail
     * @purpose To send email
     * @param array $parms
     * @return boolean
     */
    private function sendMail($parms) {

        $this->mail->ContentType = isset($parms['ContentType']) ? $parms['ContentType'] : 'text/html';
        $this->mail->From = 'info@abc.com';
        $this->mail->FromName = 'abc';
        if (strpos($parms['To'], ',') !== false || strpos($parms['To'], ';') !== false) {
            $parms['To'] = explode(",", $parms['To']);
        }
        if (is_array($parms['To'])) {
            foreach ($parms['To'] as $email) {
                $this->mail->AddAddress($email);
            }
        } else {
            $this->mail->AddAddress($parms['To']);
        }
        if (isset($parms['From'])) {
            $this->mail->AddReplyTo($parms['From'], $this->mail->FromName);
        }
        $this->mail->Subject = isset($parms['Subject']) ? $parms['Subject'] : 'abc';
        $this->mail->msgHTML(isset($parms['Body']) ? $parms['Body'] : '');
         if ($result = $this->mail->Send())
            return $result;
        else
            return FALSE;
    }

    public function register_post() {
        $this->api_manager->handle_request();
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
   //check if form is not validated then return all errors
        if ($this->form_validation->run() == false) {
                $this->response(array(
                    'status' => '0',
                    'message' => explode("\n", validation_errors())
                        ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                die();
       } else {

            $user['email'] = $this->api_manager->parse_request('email');
            $user['password'] = $this->api_manager->parse_request('password');
            $user['first_name'] = $this->api_manager->parse_request('first_name');
            $user['last_name'] = $this->api_manager->parse_request('last_name');
            $device_token = $this->api_manager->parse_request('device_token');
            $user['user_type'] = 'app';
            $user['status'] = 'pending_approval';
            $data['user_otp'] = rand('123456', '987654');
            $data['name'] = $user['first_name'];
            $mail_params['ContentType'] = "text/html";
            $mail_params['To'] = $user['email'];
            $mail_params['Subject'] = 'Verification';
            $mail_params['Body'] = $this->load->view('email_format/otp_email', $data, true);
            $user1 = array('email'=>$user['email']);
            $userDetails = $this->users_model->select_record($user1,'users');
              if (!empty($userDetails)) {
                  if($userDetails['is_searchable'] == '1'){
                if ($userDetails['status'] == 'pending_approval') {
                    $this->response(array(
                        'status' => '0',
                        'message' => "You are already registered. Please confirm you account.",
                            ), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array(
                        'status' => '0',
                        'message' => "User registered already",
                            ), REST_Controller::HTTP_OK);
                  }}
             else {
                 if ($response = $this->sendMail($mail_params)) {
                    $user['otp'] = $data['user_otp'];
                    $user['otp_created'] = date('Y-m-d H:m:i');
                    $user['is_searchable'] = '1';
                    $user_id = $this->users_model->update($user,$user1);
                    $this->response(array(
                        'status' => '1',
                        'message' => "User created successfully",
                        'data' => array('email' => $user['email']),
                            ), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array(
                        'status' => '0',
                        'message' => "Error in registration process. Please try again.",
                            ), REST_Controller::HTTP_OK);
                }
              } 
        } else{
            if ($response = $this->sendMail($mail_params)) {
                    $user['otp'] = $data['user_otp'];
                    $user['otp_created'] = date('Y-m-d H:m:i');
                    $user_id = $this->users_model->save($user);
                  $this->response(array(
                        'status' => '1',
                        'message' => "User created successfully",
                        'data' => array('email' => $user['email']),
                            ), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array(
                        'status' => '0',
                        'message' => "Error in registration process. Please try again.",
                            ), REST_Controller::HTTP_OK);
                }
        }
    }}

    public function verify_OTP_post() {
        $this->api_manager->handle_request();
        $status = '0';
        $message = '';
        $response_data = array();
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('otp', 'OTP', 'trim|required');
        $this->form_validation->set_rules('device_id', 'Device ID', 'trim');


        //check if form is not validated then return all errors
        if ($this->form_validation->run() == false) {
              $message = explode("\n", validation_errors());
        } else {
            $email = $this->api_manager->parse_request('email');
            $otp = $this->api_manager->parse_request('otp');
            $build = $this->api_manager->parse_request('build');
            $device_token = $this->api_manager->parse_request('device_id');
            $build = ($build == 'test') ? 'test' : 'live';
            $email1 = array('email'=>$email);
            $userDetails = $this->users_model->select_record($email1,'users');
            if (!empty($userDetails) && $userDetails['status'] == 'pending_approval') {
                $diff_time = (strtotime(date('Y-m-d H:m:i')) - strtotime($userDetails['otp_created'])) / 60;
                if($otp && $otp != $userDetails['otp']){
                    $message = 'Token is not valid.';
                } else if($diff_time <= 10) {
                    $user['otp'] = '';
                    $user['status'] = 'active';
                    $userDetails1 = array('id'=>$userDetails['id']);
                    $this->users_model->update($user, $userDetails1);
                    $token = $this->api_manager->createAccessToken($userDetails['id'], $device_token, $build);
                    $data['first_name'] = $userDetails['first_name'];
                    $data['last_name'] = $userDetails['last_name'];
                    $data['email'] = $userDetails['email'];
                    $userDetails['profile_image'] = '';
                    $userDetails['image_path'] = '';
                    if ($userDetails['profile_image'] && file_exists(FCPATH . 'uploads/profile_image/' . $userDetails['profile_image'])) {
                        $data['profile_image'] = $userDetails['profile_image'];
                        $data['image_path'] = base_url('uploads/profile_image/' . $userDetails['profile_image']);
                    }
                    $status = '1';
                    $message = 'Your account is activated.';
                    $data['token'] = $token['access_token'];
                    $response_data = $data;
                } else {
                    $message = 'Token expired.';
                }
            } else  if (!empty($userDetails) && $userDetails['status'] == 'active') {
                //if user does not exist
                $message = 'Account is already activated';
            } else  {
                //if user does not exist
                $message = 'Email is not correct.';
            }
        }
        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $response_data
                ), REST_Controller::HTTP_OK);
    }

    public function send_OTP_post() {
        $this->api_manager->handle_request();
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        //check if form is not validated then return all errors
        if ($this->form_validation->run() == false) {
            $this->response(array(
                    'status' => '0',
                    'message' => explode("\n", validation_errors())
                        ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                die();
            
        } else {
            $email = $this->api_manager->parse_request('email');
            $request_type = $this->api_manager->parse_request('request_type');
            $email1 = array('email'=>$email);
            $userDetails = $this->users_model->select_record($email1,'users');
            if (!empty($userDetails)) {
                if ($request_type == 'forgot_password' && $userDetails['status'] == 'pending_approval') {
                    $this->response(array(
                        'status' => '0',
                        'message' => 'Your account is not active.'
                            ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                } else {
                    $data['user_otp'] = rand('123456', '987654');
                    $data['name'] = $userDetails['first_name'];
                    $mail_params['ContentType'] = "text/html";
                    $mail_params['To'] = $email;
                    $mail_params['Subject'] = 'Verification';
                    $mail_params['Body'] = $this->load->view('email_format/otp_email', $data, true);
                    if ($response = $this->sendMail($mail_params)) {
                        $user['otp'] = $data['user_otp'];
                        $user['otp_created'] = date('Y-m-d H:m:i');
                       $userDetails1 = array('id'=>$userDetails1['id']);
                       $this->users_model->update($user, $userDetails1);
                        $this->response(array(
                            'status' => '1',
                            'message' => "resend otp successfully",
                            'data' => array('email' => $email),
                                ), REST_Controller::HTTP_OK);
                    } else {
                        echo $mail->ErrorInfo;
                        $this->response(array(
                            'status' => '0',
                            'message' => 'Error in sending email.'
                                ), REST_Controller::HTTP_OK);
                    }
                }
            } else {
                //if user does not exist
                $this->response(array(
                    'status' => '0',
                    'message' => 'Email is not correct'
                        ), REST_Controller::HTTP_OK);
            }
        }
    }

    public function login_post() {
       $this->api_manager->handle_request();
        $status = '0';
        $message = '';
        $response_data = array();
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('uuid', 'uuid', 'trim');
        $this->form_validation->set_rules('platform', 'platform', 'trim');
       //check if form is not validated then return all errors
        if ($this->form_validation->run() == false) {
           $message = explode("\n", validation_errors());
       } else {
            $where['email'] = $this->api_manager->parse_request('email');
            $where['password'] = $this->api_manager->parse_request('password');
            $device_token = $this->api_manager->parse_request('uuid');
            $platform = $this->api_manager->parse_request('platform');
            $build = $this->api_manager->parse_request('build');
            $build = ($build == 'test') ? 'test' : 'live';
            $userDetails = $this->users_model->select_record($where,"users");
           
            if (!empty($userDetails)) {
                if ($userDetails['status'] == 'pending_approval') {
                    $message = 'Your account is not active.';
                    $status = '1';
                } else {
                    $token = $this->api_manager->createAccessToken($userDetails['id'], $device_token,$build,$platform);
                    $this->db->update('users', array('last_login' => date('Y-m-d H:i:s')), array('id' => $userDetails['id']));
                    $message = 'Congratulations! you are successfully logged in.';
                    $status = '1';
                    $userDetails['image_path'] = '';
                    if ($userDetails['profile_image'] && file_exists(FCPATH . 'uploads/profile_images/' . $userDetails['profile_image'])) {
                       
                        $userDetails['image_path'] = base_url('uploads/profile_images/' . $userDetails['profile_image']);
                    }
                     
                    $userDetails['token'] = $token;
                    $response_data['id'] = $userDetails['id'];
                    $response_data['first_name'] = $userDetails['first_name'];
                    $response_data['last_name'] = $userDetails['last_name'];
                    $response_data['profile_image'] = $userDetails['image_path'];
		    $response_data['status'] = $userDetails['status'];
		    $response_data['email'] = $userDetails['email'];
		    $response_data['token'] = $userDetails['token']['access_token'];
                }
            } else {
                //if user does not exist
                $message = 'Email or password is not correct.';
            }
        }
        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $response_data
                ), REST_Controller::HTTP_OK);
    }

    function forgot_password_post() {
        $this->api_manager->handle_request();
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        if ($this->form_validation->run() == false) {
             $this->response(array(
                    'status' => '0',
                    'message' => explode("\n", validation_errors())
                        ), REST_Controller::HTTP_OK);
       } else {
            $email = $this->api_manager->parse_request('email');
            $email1 = array('email'=>$email);
            $userDetails = $this->users_model->select_record($email1,'users');
            /* IF VALID REQUEST */
            /* CHECK EMAIL IS REGISTERED OR NOT WITH STATUS = 1 */
            if ($userDetails) {
                /* CHECK STATUS */
                if ($userDetails['status'] == 'active') {
                    /* GENERATE A RANDOM PASSWORD AND SAVE IT IN USERS RECORD */
                    $user['password'] = $this->users_model->generate_random_string(10);
                    $userDetails1 = array('id'=>$userDetails['id']);
                    $this->users_model->update($user, $userDetails1);
                    /* EMAIL PASSWORD TO USER */
                    /* EMAIL OTP TO THE USER */
                    $mail_params['ContentType'] = "text/html";
                    $mail_params['To'] = $email;
                    $mail_params['Subject'] = 'StringFlixPassword Notification';
                    $mail_params['Body'] = $this->load->view('email_format/forgot_password_email', array('user' => $userDetails, 'password' => $user['password']), true);

                    if ($this->sendMail($mail_params)) {
                        $this->response(array(
                            'status' => '1',
                            'message' => 'Email Sent successfully'
                                ), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array(
                            'status' => '1',
                            'message' => 'Email sending Failed'
                                ), REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response(array(
                        'status' => '0',
                        'message' => 'Confirmation Pending'
                            ), REST_Controller::HTTP_OK);
                    
                }
            } else {
                $this->response(array(
                    'status' => '0',
                    'message' => 'User does not exist'
                        ), REST_Controller::HTTP_OK);
            }
        }
    }

    public function upload_video_post() {
        $this->api_manager->handle_request();
        $this->form_validation->set_rules('project_id', 'Project_id', 'trim|required');
        //$data = $this->input->post();
        //Authenticatio of the users
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
                $this->response(array(
                    'status' => '0',
                    'message' => explode("\n", validation_errors())
                        ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                die();
       }else{
       $project_id = $this->api_manager->parse_request('project_id');
        //validate the file and check the extensions
        $allowedExt = array("mp4","3gpp","3gp","mkv","wmv","avi","mov","qt","x-flv","flv","x-matroska","x-msvideo","x-ms-wmv","quicktime","quicktime");
        $min_size = NULL;
        $max_size = NULL;
        $validationError = $this->custom_file->validateFile("video",$max_size,$min_size,$allowedExt);
        if (!$validationError) {
            $ext = pathinfo($_FILES['video']["name"], PATHINFO_EXTENSION);
            $filename = $user_details['user_id'] . '.' .$ext;
            $path = FCPATH . 'uploads/videos/' . $project_id;
            $url = base_url('uploads/videos/' . $project_id."/".$filename);
            $target = $path . "/" . $filename;
            //create the directory if does not exist
            $this->custom_file->mkdir_r($path);
            $data1['url'] = $url;
            $data1['user_name'] = $user_details['first_name'] . " " . $user_details['last_name'];
            $user_video['user_id'] = $user_details['user_id'];
            $user_video['video_name'] = $filename;
            $data = array('video'=>$filename);
            $where = array('user_id'=>$user_details['user_id'],'project_id'=>$project_id);
            if (file_exists($path)) {
                //upload video in a directory
                if (move_uploaded_file($_FILES['video']['tmp_name'], $target)) {
                    //upload video in database
                    if($this->projects_model->fetch_user_project_data($where)){
                    if($this->users_model->update_project_users($data,$where)){
                    $this->response(array(
                        'status' => '1',
                        'message' => 'video upload successfully',
                        'data'=>$data1,
                          ), REST_Controller::HTTP_OK);
                    } else{
                        $this->response(array(
                        'status' => '0',
                        'message' => 'video not upload',
                            ), REST_Controller::HTTP_OK);  
                    }
                    }else{
                      $this->response(array(
                        'status' => '0',
                        'message' => 'invalid request',
                         ), REST_Controller::HTTP_OK);  
                    }
                }
            }
        } else {
            $this->response(array(
                'status' => '0',
                'message' => $validationError,
                ), REST_Controller::HTTP_OK);
        }
       }  
    }
    function edit_post(){
       $this->api_manager->handle_request(); 
       if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $allowedExt = array("jpg","jpeg","png");
        $min_size = NULL;
        $max_size = NULL;
        $validationError = $this->custom_file->validateFile("image",$max_size,$min_size,$allowedExt);
        if (!$validationError) {
            $ext = pathinfo($_FILES['image']["name"], PATHINFO_EXTENSION);
            $filename = $user_details['user_id'] . '.' .$ext;
        
       $path = FCPATH . 'uploads/profile_images/' ;
        $url = base_url('uploads/profile_images/'.$filename);
        $target = $path . "/" . $filename;
        
        $data1['url'] = $url;
        $data = array('profile_image'=>$filename);
        
        $where = array('id'=>$user_details['user_id']);
          
                //upload video in a directory
                if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
                    //upload video in database
                    if($this->users_model->select_record($where,'users')){
                    if($this->users_model->update($data,$where)){
                    $this->response(array(
                        'status' => '1',
                        'message' => 'image upload successfully',
                        'data' => $data1
                            ), REST_Controller::HTTP_OK);
                    } else{
                        $this->response(array(
                        'status' => '0',
                        'message' => 'image not upload',
                         ), REST_Controller::HTTP_OK);  
                    }
                    }else{
                      $this->response(array(
                        'status' => '0',
                        'message' => 'invalid request',
                        ), REST_Controller::HTTP_OK);  
                    }
                }
           
        }else{
           $this->response(array(
                'status' => '0',
                'message' => $validationError,
                ), REST_Controller::HTTP_OK);
        }
    }
    function update_password_post(){
       $this->api_manager->handle_request();
       if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
       $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required');
       $this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
        if ($this->form_validation->run() == false) {
                $this->response(array(
                    'status' => '0',
                    'message' => explode("\n", validation_errors())
                        ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                die();
       } else{
           $old_password = $this->api_manager->parse_request('old_password');
           $new_password = $this->api_manager->parse_request('new_password');
           $where = array('id'=>$user_details['user_id'],'password'=>$old_password);
           $data = array('password'=>$new_password);
           $result = $this->users_model->select_record($where,'users');
           if($result){
               $where1 = array('id'=>$user_details['user_id']);
              if($this->users_model->update($data,$where1)){
                  $this->response(array(
                        'status' => '1',
                        'message' => 'password update succesfully',
                         ), REST_Controller::HTTP_OK);  
               }else{
                    $this->response(array(
                        'status' => '0',
                        'message' => 'password not update',
                         ), REST_Controller::HTTP_OK);  
               }
           }else{
               $this->response(array(
                        'status' => '0',
                        'message' => 'old password does not match',
                         ), REST_Controller::HTTP_OK);  
           }
       }
       
       
    }
    function count_post(){
      $this->api_manager->handle_request();
       if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $data = array('to'=>$user_details['user_id'],'is_read'=>'0',);
       $message_count = $this->message_model->count_message($data);
       $data1 = array('send_to'=>$user_details['user_id'],'is_read'=>'0');
       $notification_count = $this->notification_model->count_results($data1);
       $res['msg_count'] = $message_count;
       $res['notification_count'] = $notification_count;
       $this->response(array(
                        'status' => '1',
                        'message' => 'Get count successfully',
                         'data'=>$res
                         ), REST_Controller::HTTP_OK);  
    }
    function logout_post(){
        $this->api_manager->handle_request();
       if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $where = array('user_id'=>$user_details['user_id']);
        if($this->users_model->logout($where)){
          $this->response(array(
                'status' =>'1',
                'message' => 'logout successfully'
                    ), REST_Controller::HTTP_OK);  
        }else{
             $this->response(array(
                'status' =>'0',
                'message' => 'not logout'
                    ), REST_Controller::HTTP_OK); 
        }
    }

}