<?php

class Message_Model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'message';
        $this->validate = array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required',
            ),
        );
    }
    function insert_message_recipient($data = array()){
        if ($this->db->insert('message_recipients', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }  
    }
    function select_message_recipient($where = array()){
                $this->db->select('mr.msg_id,mr.from,m.msg,u.first_name,u.profile_image,u.last_name,m.created,mr.to,mr.is_read');
                $this->db->from('message_recipients mr');
                $this->db->join('message m', 'm.msg_id = mr.msg_id','INNER');
                $this->db->join('users u', 'u.id = mr.from','INNER');
                $this->db->where('mr.group_id',$where['project_id']);
                $this->db->where('mr.type',$where['type']);
                $this->db->group_by('msg_id');
                $getdata = $this->db->get();
    if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return false;
        }   
    }
     function batch_query($data = array(array())){
        $query = $this->db->insert_batch('message_recipients', $data); 
        return $query;
    }  
     function update_message_recipiente($data = array(), $where = array()) {
        if (isset($data['password'])) {
            $data['password'] = $this->hash_this($data['password']);
        }
        $query = $this->db->update('message_recipients', $data, $where);
        return $query;
    }
    function count_message($where = array()){
     if (!is_null($where) && !empty($where)) {
            $this->db->where('to',$where['to']);
            $this->db->where('is_read',$where['is_read']);
            $this->db->where_not_in('from',$where['to']);
        }
        $getData = $this->db->count_all_results('message_recipients');
        return $getData;   
    }
}

