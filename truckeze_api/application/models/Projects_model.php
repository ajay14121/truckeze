<?php

class Projects_Model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'projects';
        $this->validate = array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required',
            ),
        );
    }

    function project_users($data = array()) {
        if ($this->db->insert('project_users', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    function fetch_user_project_data($data = array()) {

        $getdata = $this->db->get_where('project_users', $data);
        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return false;
        }
    }

    function delete_users($data = array()) {
        if ($this->db->delete("project_users", $data))
            return true;
        else
            return false;
    }

    function update_project_user($data = array(), $invitation_id = NULL) {
        return $this->db->update("project_users", $data, array('id' => $invitation_id));
    }

    function get_projects($where = array()) {
        $this->db->select('projects.project_name,project_users.is_owner,projects.deadline,project_users.project_id');
        $this->db->from('project_users');
        $this->db->join('projects', 'projects.project_id = project_users.project_id', 'INNER');
        $this->db->where($where);
        $query = $this->db->get();
        //echo  $this->db->last_query(); 
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        }
        return false;
    }

    function project_details($project_id) {
        $this->db->select('pu.user_id,u.first_name,u.last_name,p.project_name,p.deadline,pu.is_accept,pu.is_owner,pu.user_id,pu.video,u.profile_image');
        $this->db->from('projects p');
        $this->db->join('project_users pu', 'pu.project_id = p.project_id', 'INNER');
        $this->db->join('users u', 'u.id = pu.user_id', 'INNER');
        $this->db->where('p.project_id', $project_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        }
        return false;
    }

    /*
     * Function by use we can reterive all invites of project for an user 
     */

    function project_invites($user_id) {
        $this->db->select('pu.user_id,pu.id as invitation_id,p.project_name,p.deadline,pu.is_accept');
        $this->db->from('projects p');
        $this->db->join('project_users pu', 'pu.project_id = p.project_id', 'INNER');
        $this->db->where('pu.user_id', $user_id);
        $this->db->where('pu.is_owner', '0');
        $this->db->where('pu.is_accept', '0');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        }
        return false;
    }

}
