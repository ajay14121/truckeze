<?php

/**
 * API manager for complete solution of HTTP requests.
 * Specially baked for Codeigniter.
 * 
 * @Created By  :   Hanish Singla
 * @Created At  :   2015-07-01
 */
class API_Manager {

    public $version = '1.1';
    protected $statusCode = 200;
    protected $statusText;
    protected $parameters = array();
    protected $httpHeaders = array();
    public $requestHeaders = array();
    public $request = array();
    protected $statusTexts = array();
    private $tbl_access_token = 'auth_access_tokens';

    public function __construct() {
        $this->requestHeaders = getallheaders();

        $CI = & get_instance();
        // loading registration model for selecting all states from table for client registration
        $loadedmodels = array();
        $CI->load->model($loadedmodels);

        $libraries = array(
            'form_validation',
        );

        $CI->load->library($libraries);

        $CI->form_validation->set_error_delimiters('', '');
    }

    private function get_db_instance() {
        $CI = & get_instance();
        return $CI->db;
    }

    private function _fetch_from_array(&$array, $index = '', $xss_clean = FALSE) {
        $CI = & get_instance();
        if (!isset($array[$index])) {
            return FALSE;
        }
        if (strtolower($array[$index]) == 'null' || is_null($array[$index])) {
            return FALSE;
        }
        if (is_object($array[$index])) {
            $array[$index] = (array) $array[$index];
        }

        if ($xss_clean === TRUE) {
            if (is_array($array[$index])) {
                foreach ($array[$index] as $key => $value) {
                    $array[$index][$key] = $CI->security->xss_clean($value);
                }
                return $array[$index];
            } else {
                return $CI->security->xss_clean($array[$index]);
            }
        }
        return $array[$index];
    }

    /**
     * Handles HTTP request and put request data in global
     * variables. Makes log of request data.
     *
     */
    public function handle_request() {
        $CI = & get_instance();
        
        $files = count($_FILES)?json_encode($_FILES):"";
        if ($CI->input->post()) {
            $_REQUEST = $_POST;
            $_GET = $_REQUEST;
            $this->request = $_REQUEST;
            error_log('POST RAW : ' . date('Y-m-d H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . json_encode($_REQUEST) .PHP_EOL.' FILE DATA : '.$files.PHP_EOL.' /HEADERS/ ' . json_encode($this->requestHeaders) . PHP_EOL . PHP_EOL, 3, 'error_log_custom.txt');
        } elseif ($CI->input->get()) {
            $_REQUEST = $_GET;
            $_POST = $_REQUEST;
            $this->request = $_REQUEST;
            error_log('GET RAW : ' . date('Y-m-d H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . json_encode($_REQUEST) . PHP_EOL.' FILE DATA : '.$files.PHP_EOL.' /HEADERS/ ' . json_encode($this->requestHeaders) . PHP_EOL . PHP_EOL, 3, 'error_log_custom.txt');
        } else {
            $_REQUEST = (array) json_decode(stripcslashes(file_get_contents('php://input')), TRUE);
            $_POST = $_REQUEST;
            $_GET = $_REQUEST;
            $this->request = $_REQUEST;
            error_log('POST Mobile : ' . date('Y-m-d H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . json_encode($_REQUEST) . PHP_EOL.' FILE DATA : '.$files.PHP_EOL.' /HEADERS/ ' . json_encode($this->requestHeaders) . PHP_EOL . PHP_EOL, 3, 'error_log_custom.txt');
        }
        
    }

    /**
     * Parses index data from HTTP request data.
     * Takes an index and returns data present on that index.
     *
     * @param $index string
     * Index needed to be parsed from request data.
     * 
     * @param $xss_clean Boolean
     * Clean post data for cross-server scripts.
     * 
     * @return
     * Post data variables.
     *
     */
    public function parse_request($index, $xss_clean = TRUE) {
        // Check if a field has been provided
        if ($index === NULL AND ! empty($_REQUEST)) {
            $post = array();
            // Loop through the full _POST array and return it
            foreach (array_keys($_REQUEST) as $key) {
                $post[$key] = $this->_fetch_from_array($_REQUEST, $key, $xss_clean);
            }

            return $post;
        }
        return $this->_fetch_from_array($_REQUEST, strtolower($index), $xss_clean);
    }

    /**
     * Outputs data to response of HTTP request.
     *
     * @param $param
     * Response data
     * 
     */
    public function set_output($param) {
        $this->setHttpHeader('Content-Type', 'application/json');
        header(sprintf('HTTP/%s %s %s', $this->version, $this->statusCode, $this->statusText));

        foreach ($this->getHttpHeaders() as $name => $header) {
            header(sprintf('%s: %s', $name, $header));
        }

        if (!$param) {
            $param = array();
        } elseif (is_string($param) || is_numeric($param)) {
            $param['data'] = $param;
        }

        array_walk_recursive($param, 'self::filter_array');
        array_walk_recursive($param, 'self::filter_output');
        //error_log('OutPut : ' . date('Y-m-d H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . json_encode($param) . PHP_EOL . PHP_EOL, 3, 'error_log_custom.txt');
        echo json_encode($param);
    }

    public static function filter_output(&$param, $key) {
//        if (is_numeric($param)) {
//            $param = intval($param);
//        }
        if (is_null($param)) {
            $param = '';
        }
    }

    public static function filter_array(&$param, $key) {
        if (is_object($param)) {
            $param = (array) $param;
        }
    }

    /**
     * Sets status code for a HTTP request.
     * Default status code is 200 (OK)
     *
     * @param $statusCode Int
     * HTTP status code. Default 200
     * 
     * @param $error String
     * status string. Default OK
     * 
     * @param $errorDescription String
     * Error description.
     * 
     */
    public function setError($statusCode = 200, $error = 'OK', $errorDescription = null) {
        $this->setStatusCode($statusCode, $error);
        $this->addParameters(array(
            'error' => $error,
            'error_description' => $errorDescription,
        ));

        $this->addHttpHeaders(array(
            'Cache-Control' => 'no-store'
        ));
    }

    /**
     * Sets status code for a HTTP request.
     *
     * @param $statusCode Int
     * HTTP status code. Default 200
     * 
     * @param $text String
     * status string. Default OK
     * 
     */
    private function setStatusCode($statusCode, $text = null) {
        $this->statusCode = (int) $statusCode;
        $this->statusText = (null != $text) ? $text : '';
    }

    public function addParameters(array $parameters) {
        $this->parameters = array_merge($this->parameters, $parameters);
    }

    /**
     * Get HTTP headers for response.
     *
     */
    public function getHttpHeaders() {
        return $this->httpHeaders;
    }

    /**
     * Get Request headers for response.
     *
     */
    public function getRequestHeaders($header_name = NULL, $secondary_header_name = NULL) {

        if (isset($this->requestHeaders[$header_name]) && !empty($this->requestHeaders[$header_name])) {
            return $this->requestHeaders[$header_name];
        }
        if (isset($this->requestHeaders[$secondary_header_name]) && !empty($this->requestHeaders[$secondary_header_name])) {
            return $this->requestHeaders[$secondary_header_name];
        }
    }

    /**
     * Sets HTTP headers for response.
     *
     * @param $httpHeaders Array
     * HTTP Headers
     * 
     */
    public function addHttpHeaders(array $httpHeaders) {
        $this->httpHeaders = array_merge($this->httpHeaders, $httpHeaders);
    }

    /**
     * Sets single HTTP header for response.
     *
     * @param $name String
     * HTTP Header key
     * 
     * @param $value String
     * HTTP Header value
     * 
     */
    public function setHttpHeader($name, $value) {
        $this->httpHeaders[$name] = $value;
    }

    /**
     * Gets access token for user.
     *
     * @param $user_id
     * User ID associated with the access token
     *
     */
    public function getAccessToken($user_id = null) {
        $db = $this->get_db_instance();
        if (!$db) {
            $this->setError(503, 'database_not_found', 'Error while tried to access database');
            return FALSE;
        }

        $db->select('*');
        $db->from($this->tbl_access_token);
        $db->where('user_id', $user_id);
        $db->where('active', 1);
        $query = $db->get();

        if ($query->num_rows()) {
            $row = $query->row();
            $token = array(
                "access_token" => $row->access_token,
                //"request" => $this->requestHeaders,
                "token_type" => 'Auth-Token'
            );

            return $token;
        } else {
            return FALSE;
        }
    }

    /**
     * Handle the verification of access token.
     *
     * @param $email String
     * User Email associated with the access token
     *
     * @param $token String
     * access token
     *
     */
    public function verifyAccessToken($email = null, $token = null) {
        $CI = &get_instance();
        $db = $this->get_db_instance();
        if (!$db) {
            $this->setError(503, 'database_not_found', 'Error while tried to access database');
            return FALSE;
        }

        if (!$email) {
            $email = $this->getRequestHeaders('email', 'Email');
        }

        if (!$token) {
            $token = $this->getRequestHeaders('access_token', 'Access-Token');
        }

        $db->select('*');
        $db->from($this->tbl_access_token);
        $db->join('users', $this->tbl_access_token . '.user_id=users.id and users.status="active"');
        $db->where($this->tbl_access_token . '.active', 1);
        $db->where('access_token', $token);
        $db->where('email', $email);
        $query = $db->get();

         if ($query->num_rows()) {
            $result = array();
            while ($row = $query->unbuffered_row('array')) {
                $result = array(
                    'user_id' => $row['user_id'],
                    'email' => $row['email'],
                    'status' => $row['status'],
                    'first_name' => $row['first_name'],
                    'last_name' => $row['last_name'],
                    //'date_created' => $row['date_created'],
                );
            }
            return $result;
        } else {
            $this->setError(401, 'access_denied', 'Error while tried to access user');
            return FALSE;
        }
    }

    /**
     * Handle the creation of access token.
     *
     * @param $user_id
     * User ID associated with the access token
     *
     */
    public function createAccessToken($user_id = null, $device_token = NULL, $build = 'test',$platform = null) {
        $db = $this->get_db_instance();
        if (!$db) {
            $this->setError(503, 'database_not_found', 'Error while tried to access database');
            return FALSE;
        }
        if ($device_token) {
            $db->delete($this->tbl_access_token, array('device_token' => $device_token));
        }
        do {
            $token_string = $this->generateAccessToken();
        } while (!$db->insert($this->tbl_access_token, array('access_token' => $token_string, 'user_id' => $user_id, 'active' => 1, 'device_token' => $device_token, 'build' => $build,'platform' =>$platform)));

        $token = array(
            "access_token" => $token_string,
            //"request" => $this->requestHeaders,
            "token_type" => 'Auth-Token'
        );

        return $token;
    }

    /**
     * Handle the creation of access token.
     *
     * @param $user_id
     * User ID associated with the access token
     *
     */
    public function destroyAccessToken($user_id = null) {
        $CI = &get_instance();
        $db = $this->get_db_instance();

        if (!$db) {
            $this->setError(503, 'database_not_found', 'Error while tried to access database');
            return FALSE;
        }
        $token = $this->getRequestHeaders('access_token', 'Access-Token');
        $db->delete($this->tbl_access_token, array('user_id' => $user_id, 'access_token' => $token));
        return TRUE;
    }

    /**
     * Generates an unique access token.
     *
     * @return
     * An unique access token.
     *
     */
    protected function generateAccessToken() {
        $tokenLen = 40;
        $randomData = mt_rand() . mt_rand() . mt_rand() . mt_rand() . microtime(true) . uniqid(mt_rand(), true);
        return substr(hash('sha512', $randomData), 0, $tokenLen);
    }
    public function format_output($param) {
        if (!$param) {
            $param = array();
        }
        array_walk_recursive($param, 'self::filter_array');
        array_walk_recursive($param, 'self::filter_output');
        //error_log('OutPut : ' . date('Y-m-d H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . json_encode($param) . PHP_EOL . PHP_EOL, 3, 'error_log_custom.txt');
        return $param;
    }
    //send iphone push notification
    public function iphonePushNotification($deviceToken = '', $title = '', $message = '', $build = '') {
        //require_once FCPATH . 'application/libraries/PushDemo/simplepush.php';
        $message = substr($message, 0, 80);
        $result = sendPushMessage($deviceToken, $title, $message, $build);
        return $result;
    }
    //send notification to players.call from  events related  api`s
    public function androidPushNotification($device_id = '', $title = '', $msg = '') {
        $apiKey ="AIzaSyAL-vm4y39sWPe0948MX3wMwY144uMyhv0";
        $registration_id = array($device_id);
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
            'registration_ids' => $registration_id,
            'data' => array(
                "vibrate" => 1,
                "sound" => 1,
                "title" => $title,
                "message" => $msg
            )
        );

        $headers = array(
            'Authorization: key='.$apiKey,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        $t = curl_setopt($ch, CURLOPT_URL, $url);
        $t = curl_setopt($ch, CURLOPT_POST, true);
        $t = curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $t = curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $t = curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $t = curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post
        $result = curl_exec($ch);
        print_r($result);
        exit;
        curl_close($ch);
        return $result;
    }
    public function sendNotification($notification_details = array()) {
        $CI = &get_instance();
        $db = $this->get_db_instance();
        if (!$db) {
            $this->setError(503, 'database_not_found', 'Error while tried to access database');
            return FALSE;
        }
        if (!empty($notification_details)) {
            $title = $notification_details['title'];
            $message = $notification_details['message'];

            foreach ($notification_details['device_tokens'] as $key => $device_token) {
                $result = FALSE;
                if ($device_token && strtolower($notification_details['platform'][$key]) == 'ios') {
                    $result = self::iphonePushNotification($device_token, $title, $message, $notification_details['build'][$key]);
                    
                }else{
                    $result = self::androidPushNotification($device_token, $title, $message);
                    print_r($result);
                    exit;
                }
                //$db->insert('notifications', array('device_token' => $device_token, 'status' => $result, 'message' => $message, 'build_type' => $notification_details['build'][$key]));
            }
        }
        return TRUE;
    }


}

?>